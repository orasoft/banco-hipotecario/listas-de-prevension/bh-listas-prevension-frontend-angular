export interface IProfile {
    // profile_id?: number,
    // profile_rol: number,
    // profile_acceso: string,
    // profile_visualizacion: string,
    // profile_descarga: any,
    // profile_responsable: any,
    // fields?: IFieldsProfile[],
    // profile_idLista: number,
    // roles_nombre?: string,
    id: string,
    nombre: string,
    perfiles: [IFieldsProfile]
}

export interface IFieldsProfile {
    acceso: string,
    createdAt: string,
    descargar: boolean,
    id: string,
    responsable: boolean
    rol: {
        createdAt: string
        id: string
        idBhRol: number
        nombre: string
        updatedAt: string
        version: number
    }
    updatedAt: string
    version: number
    visualizar: string
}


