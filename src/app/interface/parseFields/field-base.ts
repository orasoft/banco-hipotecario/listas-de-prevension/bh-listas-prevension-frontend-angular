
export class FieldBase<T> {
    name: string
    index?: string
    label: string;
    type: string;
    required: boolean;
    search: boolean;
    order: number;
    status: string;
    subcategories?: Array<any>

    constructor( options: {
        name?: string
        index?: string
        label?: string;
        type?: string;
        required?: boolean;
        search?: boolean;
        order?: number;
        status?: string;
        subcategories?: Array<any>
    } = {}) {
        this.name = options.name
        this.index = options.index
        this.label = options.label
        this.type = options.type
        this.required = options.required
        this.search = options.search
        this.order = options.order
        this.status = options.status
        this.subcategories = options.subcategories
    }
}
