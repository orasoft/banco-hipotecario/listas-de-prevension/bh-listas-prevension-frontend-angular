import {FieldBase} from './field-base';


export class TextboxField extends FieldBase<string> {
    controlType = 'textbox'
    type: string
    typeValue: string
    hidden: boolean
    validations: any[]

    constructor(options: {} = {}) {
        super(options)
        this.type = options[ 'type' ] || 'INPUT'
        this.hidden = options[ 'hidden' ] || false
        this.typeValue = options[ 'typeValue' ] || 'TEXT'
        this.validations = options[ 'validations' ] || []
    }
}
