export interface ICampos {
    busqueda: boolean,
    campo: ICampoDetalle,
    createdAt: string,
    createdBy: string,
    estado: string,
    id: string,
    orden: number,
    requerido: boolean,
    updatedAt: string,
    updatedBy: string,
    version: number
}

export interface ICampoDetalle {
    createdAt: string
    createdBy: string
    default: boolean
    descripcion: string
    formato: string
    id: string
    label: string
    nombre: string
    subcategorias: [ICampoSubcat]
    tipo: string
    updatedAt: string
    updatedBy: string
    version: number
}

export interface ICampoSubcat {
    createdAt: string
    createdBy: string
    id: string
    label: string
    updatedAt: string
    updatedBy: string
    valor: string
    version: number
}
