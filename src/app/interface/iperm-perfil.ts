export interface IPermPerfil {
    id: string,
    acceso: string,
    visualizar: string,
    descargar: boolean,
    responsable: boolean,
    version: number,
    createdAt: string,
    updatedAt: string
}
