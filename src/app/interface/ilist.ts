import {IManager} from './imanager';
import {ILabel} from './iLabel';
import {IResponse} from './iResponse';

export interface IList {
    response?: IResponse
    data: {
        idlist: string;
        description: string;
        status: boolean;
        managers: Array<IManager>
        estructura: {
            fields: Array<IFileds>
            nivCrits: INivCrit
        }
        observaciones: string
        publica: boolean
    }
}

export interface IFileds {
    index?: number
    label: string;
    type: string;
    required: boolean;
    search: boolean;
    order: number;
    status: boolean;
    subcategories?: Array<ISubCat>
}

export interface ISubCat {
    label: string
    valor: string
}

export interface INivCrit {
    msgDft: string
    nivMsg: Array<ILabel>
}
