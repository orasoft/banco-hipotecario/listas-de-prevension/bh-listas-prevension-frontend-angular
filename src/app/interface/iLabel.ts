export interface ILabel {
    id: number
    label: string
}
