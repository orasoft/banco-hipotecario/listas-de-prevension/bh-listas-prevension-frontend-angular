export interface IMenu {
    id: string,
    idPrincipal?: string,
    name: string,
    type: string,
    url: string,
    icon: string
}
