import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';
import {AppError404Component} from './views/errors/app-error-404/app-error-404.components';
import {AuthGuardService} from './services/auth-guard-service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: SimpleLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: './views/loginForm/login.module#LoginModule'
      }
    ]
  },
  {
    path: 'app',
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule',
        data: {
          title: 'Inicio'
        }
      },
      {
        path: 'admin',
        loadChildren: './views/admin/admin.module#AdminModule'
      },
      {
        path: 'upload',
        loadChildren: './views/upload/upload.module#UploadModule'
      },
      {
        path: 'reports',
        loadChildren: './views/reports/reports.module#ReportsModule'
      }
    ],
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    component: SimpleLayoutComponent,
    children: [
      {
        path: '**',
        component: AppError404Component
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
