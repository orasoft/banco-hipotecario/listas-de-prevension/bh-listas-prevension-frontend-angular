import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConexListRoutingModule} from './conex-list-routing.module';
import { ConexListParamsComponent } from './conex-list-params/conex-list-params.component';
import {DataTablesModule} from 'angular-datatables';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ConexListRoutingModule,
        DataTablesModule
    ],
    declarations: [ConexListParamsComponent],
})

export class ConexListModule {}
