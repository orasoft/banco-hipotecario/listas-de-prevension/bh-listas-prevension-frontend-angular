import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConexListParamsComponent} from './conex-list-params/conex-list-params.component';


export const routes: Routes = [
    {
        path: 'conex-list-ext',
        component: ConexListParamsComponent,
        data: {
            title: 'Conexion a Listas Externas'
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ConexListRoutingModule {
}
