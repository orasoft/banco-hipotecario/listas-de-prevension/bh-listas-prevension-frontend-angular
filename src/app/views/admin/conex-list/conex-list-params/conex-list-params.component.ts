import {Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {ApiService} from '../../../../services/apiService';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'app-conex-list-params',
  templateUrl: './conex-list-params.component.html',
  styleUrls: ['./conex-list-params.component.scss']
})
export class ConexListParamsComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtLanguage: DataTables.LanguageSettings = {
        search: 'Buscar',
        lengthMenu: 'Mostrar _MENU_ registros por página',
        info: 'Mostrando página _PAGE_ de _PAGES_',
        infoFiltered: '(Filtrado de _MAX_ registros)',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
        emptyTable: 'No se encontraron resultados'
    };

    dtTrigger: Subject<any> = new Subject();

  public TITLE = 'Conexión a Listas Externas';
    public modalRef: BsModalRef;
    public valueList
    // public listCatalog: IListaCatalog[]
    public listCatalog = []
    public valueUrl: string
    public conexList = [];
    public  idValueEdit: number
    public urlValueEdit: string;
    public deleteConex

  constructor(private modalService: BsModalService,
              private apiService: ApiService,
              public toastr: ToastrManager,
  ) { }

  ngOnInit() {
      this.dtOptions = {
          pagingType: 'full_numbers',
          language: this.dtLanguage,
          stateSave: false,
          ordering: false
      };
      this.apiService.getListasExternas().subscribe( data => {
          // console.log(data)
          this.listCatalog = data.items
      })
      this.getAllConexListInit()
  }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    public getAllConexListInit() {
        this.apiService.getListasConex().subscribe( data => {
            console.log(data)
            this.conexList = data.items
            this.dtTrigger.next();
        })
    }

  public getAllConexList() {
     this.apiService.getListasConex().subscribe( data => {
         console.log(data)
         this.conexList = data.items
         this.rerender();
     })
  }

  public openModal(template) {
      this.valueUrl = ''
      this.modalRef = this.modalService.show(template, {
          class: 'modal-lg modal-primary',
          backdrop: true,
          ignoreBackdropClick: true,
          keyboard: false
      })
  }

    public openEditModal(template, id, idLista, urlConex) {
        this.idValueEdit = id;
        this.valueList = idLista;
        this.urlValueEdit = urlConex;
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public EditConex() {
      const body = {
          id: this.idValueEdit,
          idLista: this.valueList,
          urlConexion: this.urlValueEdit
      }

      this.apiService.updateConexList(body).subscribe( data => {
          this.toastr.successToastr('Conexión actualizada con éxito', this.TITLE);
          this.modalRef.hide();
          this.getAllConexList()
      }, error1 => {
          this.toastr.errorToastr('No se pudo actuaizar la conexión', this.TITLE);
      })
    }

  public createConexList() {
      if (this.valueList !== undefined && this.valueUrl !== undefined && this.valueUrl !== '') {
          if (this.conexList.find( x => x.lista.id === this.valueList ) === undefined) {

              const body = {
                  listaId: this.valueList,
                  url: this.valueUrl
              }

              this.apiService.crearConexList(body).subscribe(data => {
                  this.toastr.successToastr('Conexión creada con éxito', this.TITLE);
                  this.modalRef.hide();
                  this.getAllConexList()
              }, error1 => {
                  this.toastr.errorToastr('No se pudo crear la conexión', this.TITLE);
              })

          } else {
              this.toastr.warningToastr('La lista seleccionada ya se encuentra asociada', this.TITLE);
          }
      } else {
          this.toastr.warningToastr('Complete todos los campos', this.TITLE);
      }
  }

  public closeModal() {
      this.modalRef.hide();
  }

  public modalDelete(id, template) {
        this.deleteConex = id
      this.modalRef = this.modalService.show(template, {
          class: 'modal-primary',
          backdrop: true,
          ignoreBackdropClick: true,
          keyboard: false
      })
  }

  public eliminar() {
      this.apiService.eliminarListasConex(this.deleteConex).subscribe( data => {
          this.toastr.successToastr('Conexión eliminada con éxito', this.TITLE);
          this.closeModal()
          this.getAllConexList();
      }, error1 => {
          this.toastr.errorToastr('No se pudo eliminar la conexión', this.TITLE);
      })
  }

}
