import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConexListParamsComponent } from './conex-list-params.component';

describe('ConexListParamsComponent', () => {
  let component: ConexListParamsComponent;
  let fixture: ComponentFixture<ConexListParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConexListParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConexListParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
