import {NgModule} from '@angular/core';
import {AsigPerfilesListComponent} from './asig-perfiles-list/asig-perfiles-list.component';
import {AsigPerfilesRoutingModule} from './asig-perfiles-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DataTablesModule} from 'angular-datatables';


@NgModule({
    imports: [
        AsigPerfilesRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DataTablesModule
    ],
    declarations: [AsigPerfilesListComponent]
})

export class AsigPerfilesModule {
}
