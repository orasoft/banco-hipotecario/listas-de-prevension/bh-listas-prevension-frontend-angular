import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {ApiService} from '../../../../services/apiService';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IRoleCatalog} from '../../../../interface/catalogs/role-catalog';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IFileds, IList} from '../../../../interface/ilist';
import {IFieldsProfile, IProfile} from '../../../../interface/iprofile';
import {ILista} from '../../mantto-listas/interfaces/ilista';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-asig-perfiles-list',
  templateUrl: './asig-perfiles-list.component.html',
  styleUrls: ['./asig-perfiles-list.component.scss']
})
export class AsigPerfilesListComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtLanguage: DataTables.LanguageSettings = {
    search: 'Buscar',
    lengthMenu: 'Mostrar _MENU_ registros por página',
    info: 'Mostrando página _PAGE_ de _PAGES_',
    infoFiltered: '(Filtrado de _MAX_ registros)',
    paginate: {
      first: 'Primero',
      last: 'Último',
      next: 'Siguiente',
      previous: 'Anterior'
    },
    emptyTable: 'No se encontraron resultados'
  };

  dtTrigger: Subject<any> = new Subject();

  public TITLE = 'Asignación de Perfiles'

  public modalRef: BsModalRef
  // public listCatalog: IListaCatalog[]
  public listCatalog = []
  public addProfileForm: FormGroup
  public editProfileForm: FormGroup
  public listRoles: IRoleCatalog
  public valueList
  public createStatus = true
  // public listaPP: IList
  public listaPP
  public fieldsProfile: IFieldsProfile[]
  public isChecked
  public profile: IProfile = new class implements IProfile {
    id: string;
    nombre: string;
    perfiles: [IFieldsProfile];
  }
  public fieldsProf = [];
  public camposAuth = [];
  public profEdit: IFieldsProfile
  public camposPerfEdit = []

  constructor(
      private modalService: BsModalService,
      public router: Router,
      private apiService: ApiService,
      private fb: FormBuilder,
      private toastr: ToastrManager
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: this.dtLanguage,
      stateSave: false,
      ordering: false
    };
    this.buildForm();
    this.buildFormEdit();
    this.apiService.todasListas().subscribe( data => {
      this.listCatalog = data.items
    })

    this.apiService.getRolesList().subscribe( data => {
      this.listRoles = data;
    })
  }

  ngAfterViewInit(): void {this.dtTrigger.next()}

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  public selectList() {
    console.log(this.valueList)
    if (this.valueList !== undefined) {
      this.fieldsProf = [];
      this.getById();
      this.apiService.getListaCampos(this.valueList).subscribe(data => {
        this.toastr.successToastr('Información de la lista se ha cargado con éxito', this.TITLE);
        this.listaPP = data
        console.log(this.listaPP)
        this.listaPP.camposConfigurados.forEach( field => {
          this.fieldsProf.push({
            index: field.campo.id,
            label: field.campo.label,
            predet: field.campo.default,
            value: false,
          })
        })
        this.createStatus = false
        // this.dtTrigger.next();
      })
    } else {
      this.toastr.warningToastr('Debe seleccionar una lista', this.TITLE);
    }
  }

  public setValueField(index, value) {
    console.log(index)
    console.log(this.stringToBool(value.target.value));

    if (this.stringToBool(value.target.value) === true) {
      this.camposAuth.push(index)
    } else {
      this.camposAuth = this.camposAuth.filter( item => item !== index );
    }
    console.log(this.camposAuth);
  }

  public setValueFieldEdit(index, value) {
    console.log(index)
    console.log(this.stringToBool(value.target.value));

    const body = {
      perfilId: this.profEdit.id,
      campoId: index
    }

    if (this.stringToBool(value.target.value)) {
      this.apiService.agregarCampoPerfil( body ).subscribe( data => {})
    } else {
      this.apiService.quitarCampoPerfil( body ).subscribe( data => {})
    }

  }

  private buildForm() {
    this.addProfileForm = this.fb.group({
      profileField: ['', Validators.compose([Validators.required])],
      accessField: ['', Validators.compose([Validators.required])],
      viewField: ['', Validators.compose([Validators.required])],
      downloadField: ['', Validators.compose([Validators.required])],
      managerField: ['', Validators.compose([Validators.required])],
    })
  }

  private buildFormEdit() {
    this.editProfileForm = this.fb.group({
      profileEditField: ['', Validators.compose([Validators.required])],
      accessEditField: ['', Validators.compose([Validators.required])],
      viewEditField: ['', Validators.compose([Validators.required])],
      downloadEditField: ['', Validators.compose([Validators.required])],
      managerEditField: ['', Validators.compose([Validators.required])],
    })
  }

  public editFieldsList(template, profile: IFieldsProfile) {
    this.fieldsProf = []
    const camposConf = []
    this.apiService.getCamposPerfil(profile.id).subscribe( data => {
      data.campos.forEach( campo => {
        camposConf.push(campo.id)
      })

      this.listaPP.camposConfigurados.forEach( campos => {
        if ( camposConf.includes(campos.campo.id) ) {
          this.fieldsProf.push({
            index: campos.campo.id,
            label: campos.campo.label,
            predet: campos.campo.default,
            value: true,
          })
        } else {
          this.fieldsProf.push({
            index: campos.campo.id,
            label: campos.campo.label,
            predet: campos.campo.default,
            value: false,
          })
        }
      })
    })

    this.profEdit = profile
    this.editProfileForm.controls.profileEditField.setValue(profile.rol.nombre)
    this.editProfileForm.controls.accessEditField.setValue(profile.acceso)
    this.editProfileForm.controls.viewEditField.setValue(profile.visualizar)
    this.editProfileForm.controls.downloadEditField.setValue(profile.descargar)
    this.editProfileForm.controls.managerEditField.setValue(profile.responsable)
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
    this.addProfileForm.reset();
    // this.fieldsProf = []
    // this.camposAuth = []
  }

  public cancel() {
    this.router.navigate(['/app/home'])
  }

  public openModal(template) {

    this.fieldsProf.forEach( campo => {
        this.camposAuth.push(campo.index)
    });
    console.log(this.camposAuth);
    this.modalRef = this.modalService.show(template, {
      class: 'modal-lg modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public addProfileToList () {

    if (this.addProfileForm.valid) {
      const perfil = {
        acceso: this.addProfileForm.getRawValue().accessField,
        visualizar: this.addProfileForm.getRawValue().viewField,
        descargar: this.stringToBool(this.addProfileForm.getRawValue().downloadField),
        responsable: this.stringToBool(this.addProfileForm.getRawValue().managerField),
        listaId: this.valueList,
        rolId: this.addProfileForm.getRawValue().profileField,
        campos: this.camposAuth
      }
      this.apiService.crearPerfil(perfil).subscribe( data => {
        this.toastr.successToastr('Perfil creado con éxito', this.TITLE)
        this.getById();
        this.modalRef.hide()
        this.addProfileForm.reset();
      }, error1 => {
        this.toastr.errorToastr('No se pudo crear el perfil', this.TITLE)
      });
    } else {
      this.toastr.warningToastr('Debe completar todos los campos', this.TITLE);
    }
  }

  public editProfileToList() {
    if (this.editProfileForm.valid) {
      const profEdit = {
        id: this.profEdit.id,
        acceso: this.editProfileForm.getRawValue().accessEditField,
        visualizar: this.editProfileForm.getRawValue().viewEditField,
        descargar: this.stringToBool(this.editProfileForm.getRawValue().downloadEditField),
        responsable: this.stringToBool(this.editProfileForm.getRawValue().managerEditField),
        version: this.profEdit.version,
        createdAt: this.profEdit.createdAt,
        updatedAt: this.profEdit.updatedAt,
        listaId: this.valueList,
        rol: this.profEdit.rol
      }
      this.apiService.updatePerfiles(profEdit).subscribe( data => {
        this.toastr.successToastr('Perfil actualizado con éxito', this.TITLE)
        this.getById();
        this.modalRef.hide()
      }, error1 => {
        this.toastr.errorToastr('No se pudo actualizar el perfil', this.TITLE)
      });
    } else {
      this.toastr.warningToastr('Debe completar todos los campos', this.TITLE);
    }
  }

  public onChange() {
    this.createStatus = true
    this.fieldsProf = []
    this.camposAuth = []
  }

  public getById() {
    this.apiService.getPerfiles(this.valueList).subscribe( data => {
      console.log(data)
      this.profile = data;
      this.rerender()
      console.log(this.profile)
    })
  }

  public changeCheckBox(id) {
    console.log(id);
  }

  private stringToBool(value): boolean {

    switch (value) {
      case 'true':
        return true
      case true:
        return true
      case 'false':
        return false
      case false:
        return false
    }
  }

}
