import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsigPerfilesListComponent } from './asig-perfiles-list.component';

describe('AsigPerfilesListComponent', () => {
  let component: AsigPerfilesListComponent;
  let fixture: ComponentFixture<AsigPerfilesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsigPerfilesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsigPerfilesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
