import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AsigPerfilesListComponent} from './asig-perfiles-list/asig-perfiles-list.component';


export const routes: Routes = [
    {

        path: 'list',
        component: AsigPerfilesListComponent,
        data: {
            title: 'Asignación de Perfiles - Listado'
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AsigPerfilesRoutingModule {
}
