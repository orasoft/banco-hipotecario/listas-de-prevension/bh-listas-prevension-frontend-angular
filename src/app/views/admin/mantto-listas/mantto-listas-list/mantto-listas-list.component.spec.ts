import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoListasListComponent } from './mantto-listas-list.component';

describe('ManttoListasListComponent', () => {
  let component: ManttoListasListComponent;
  let fixture: ComponentFixture<ManttoListasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoListasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoListasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
