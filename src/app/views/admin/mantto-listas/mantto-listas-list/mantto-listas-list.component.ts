import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';
import {combineLatest} from 'rxjs/observable/combineLatest';
import {ApiService} from '../../../../services/apiService';
import {forEach} from '@angular/router/src/utils/collection';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {IUsuarioCatalog} from '../../../../interface/catalogs/usuario-catalog';
import {IManager} from '../../../../interface/imanager';
import {IFileds, INivCrit} from '../../../../interface/ilist';
import * as moment from 'moment';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ToastrManager} from 'ng6-toastr-notifications';


@Component({
  selector: 'app-mantto-listas-list',
  templateUrl: './mantto-listas-list.component.html',
  styleUrls: ['./mantto-listas-list.component.scss']
})
export class ManttoListasListComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtLanguage: DataTables.LanguageSettings = {
    search: 'Buscar',
    lengthMenu: 'Mostrar _MENU_ registros por página',
    info: 'Mostrando página _PAGE_ de _PAGES_',
    infoFiltered: '(Filtrado de _MAX_ registros)',
    paginate: {
      first: 'Primero',
      last: 'Último',
      next: 'Siguiente',
      previous: 'Anterior'
    },
    emptyTable: 'No se encontraron resultados'
  };

  dtTrigger: Subject<any> = new Subject();

  public TITLE = 'Mantenimiento de Listas'
  public modalRef: BsModalRef
  public confirmText: string
  public confirmAction = {
    selectedId: 0,
    action: ''
  }
  public instSelected: number
  public pageSelected: number
  public listCatalog = []
  public respCatalog = []
  public valueList
  public valueResp
  public valueDate

  public listTable: any
  public motivo: string;
  public idListaBaja: string;
  public profile;

  constructor(
              private changeDetection: ChangeDetectorRef,
              private router: Router,
              private apiService: ApiService,
              private modalService: BsModalService,
              private toastr: ToastrManager
            ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: this.dtLanguage
    };
    this.pageSelected = 0
    this.findDefoult(`?listaId=*&responsableId=*&fecha=*`);
    this.apiService.todasListas().subscribe( data => {
      this.listCatalog = data.items
    })

    this.apiService.getUserCatalog().subscribe( data => {
      this.respCatalog = data
    })
  }

  ngAfterViewInit(): void {this.dtTrigger.next(); }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  public editarLista(lista) {
    if (lista.responsables.find( item => item.id === this.profile.usuario.id ) !== undefined) {
      this.router.navigate(['app/admin/mantto-listas/editar', {idLista: lista.id}]);
    } else {
      this.toastr.warningToastr('No posee permiso para editar la lista', this.TITLE);
    }
  }

  private findDefoult(values) {
    this.apiService.getListaFiltro(values).subscribe( lista => {
      this.listTable = lista
      this.rerender()
    })
  }

  public buscarLista() {
    console.log(this.valueList)
    console.log(this.valueResp)
    console.log('this.valueDate')
    console.log(this.valueDate)
    console.log('this.valueDate')

    let list = '*';
    let resp = '*';
    let date = '*';


    if (this.valueList === undefined && this.valueResp === undefined && this.valueDate === undefined) {
      this.findDefoult(`?listaId=*&responsableId=*&fecha=*`);
    } else {

      if (this.valueList !== undefined) {
        list = this.valueList
      }
      if (this.valueResp !== undefined) {
        resp = this.valueResp
      }
      if (this.valueDate !== undefined && this.valueDate !== null && this.valueDate !== '') {
        date = moment(this.valueDate, 'YYYY-MM-DD').format('DD-MM-YYYY')
      }
      this.findDefoult(`?listaId=${list}&responsableId=${resp}&fecha=${date}`);
    }

  }

  public closeModal() {
    this.modalRef.hide()
  }

  public modalAnulation(template, id) {
    this.idListaBaja = id;
    console.log(this.idListaBaja)
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public darBajaLista() {
    const body = {
      justificacion: this.motivo
    }
    this.apiService.bajaLista(this.idListaBaja, body).subscribe( data => {
      this.modalRef.hide()
      this.buscarLista()
      this.toastr.successToastr('Baja de lista efectuada con éxito', this.TITLE)
    })
  }

  public delete(id) {
    this.modalRef.hide()
  }

  public disable(id) {
    this.modalRef.hide()
  }

  public agregar() {
    this.router.navigate(['app/admin/mantto-listas/agregar'])
  }

  public agregarCampos() {
    this.router.navigate(['app/admin/mantto-listas/campos'])
  }

}
