import {Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {IFileds, IList, INivCrit, ISubCat} from '../../../../interface/ilist';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ConfirmComponentComponent} from '../../../shared/components';
import {IManager} from '../../../../interface/imanager';
import {ILabel} from '../../../../interface/iLabel';
import {ApiService} from '../../../../services/apiService';
import {NgOption} from '@ng-select/ng-select'
import {ICampoDetalle, ISubcat} from '../mantto-campos/ICampo';
import {Subject} from 'rxjs/Subject';
import {DataTableDirective} from 'angular-datatables';
import {ILista} from '../interfaces/ilista';
import {NgxSpinnerService} from 'ngx-spinner';


@Component({
    selector: 'app-mantto-listas-agregar',
    templateUrl: './mantto-listas-agregar.component.html',
    styleUrls: ['./mantto-listas-agregar.component.scss']
})
export class ManttoListasAgregarComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtLanguage: DataTables.LanguageSettings = {
        search: 'Buscar',
        lengthMenu: 'Mostrar _MENU_ registros por página',
        info: 'Mostrando página _PAGE_ de _PAGES_',
        infoFiltered: '(Filtrado de _MAX_ registros)',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
        emptyTable: 'No se encontraron resultados'
    };

    dtTrigger: Subject<any> = new Subject();

    public TITLE = 'Crear Lista'

    public modalRef: BsModalRef

    public idList: string
    public descList: string
    public statusList: string
    public extList: string

    public addFieldForm: FormGroup
    public fields: Array<IFileds> = new Array<IFileds>()
    public selectedScat = false
    public arrayItemsScat: Array<ISubCat> = new Array<ISubCat>()
    // public indexField: number
    // public indexEdit: number
    public addManager: Array<IManager> = new Array<IManager>()
    public messageDft: string
    public nivelMsg: string
    public nivelNum: number
    public arrayNivCrit: Array<ILabel> = new Array<ILabel>()

    public respSelected: NgOption;
    public serviceOptions: NgOption[];

    public campoSelected: NgOption;
    public campoOptions: NgOption[];
    public camposDet: [ICampoDetalle];
    public camposTodos: [ICampoDetalle];
    public campoView: ICampoDetalle;
    public campoEdit: ICampoDetalle;
    public camposLista = [];
    public ordenCampoView: number;
    public activeCampoView: string;
    public ordenCampoEdit: number;
    public activeCampoEdit: string;

    constructor(
        private modalService: BsModalService,
        private router: Router,
        private fb: FormBuilder,
        public toastr: ToastrManager,
        private api: ApiService,
        private spinner: NgxSpinnerService
    ) {

        // this.indexEdit = 0
        this.idList = ''
        this.descList = ''
        this.statusList = 'A'
        this.extList = 'Interna'

        this.campoView = new class implements ICampoDetalle {
            busqueda: boolean;
            createdAt: string;
            descripcion: string;
            formato: string;
            id: string;
            label: string;
            nombre: string;
            predeterminado: boolean;
            requerido: boolean;
            subcategorias: [ISubcat];
            tipo: string;
            updatedAt: string;
            version: number;
        }

        this.campoEdit = new class implements ICampoDetalle {
            busqueda: boolean;
            createdAt: string;
            descripcion: string;
            formato: string;
            id: string;
            label: string;
            nombre: string;
            predeterminado: boolean;
            requerido: boolean;
            subcategorias: [ISubcat];
            tipo: string;
            updatedAt: string;
            version: number;
        }
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            language: this.dtLanguage,
            stateSave: false,
            ordering: false
        };
        this.addFieldBuildForm();
        this.respList();
        this.camposList();
        this.camposPredetList();
    }

    public initCampoView() {
        this.campoView = new class implements ICampoDetalle {
            busqueda: boolean;
            createdAt: string;
            descripcion: string;
            formato: string;
            id: string;
            label: string;
            nombre: string;
            predeterminado: boolean;
            requerido: boolean;
            subcategorias: [ISubcat];
            tipo: string;
            updatedAt: string;
            version: number;
        }
    }

    public hideSpinner() {
        setTimeout(() =>
                this.spinner.hide(),
            5000);
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    public uploadFile() {
        this.spinner.show()
        this.hideSpinner()
        this.modalRef.hide()
    }

    public onFileChange(evt: any) {

    }

    public respList() {
        this.api.getUserCatalog().subscribe(data => {
            this.serviceOptions = data.map( resp => {
              return {
                id: resp.id,
                resp: resp.usuario
              }
            })
        })
    }

    public camposList() {
        this.api.getAllCampos().subscribe(data => {
            this.camposTodos = data.items;
            this.campoOptions = data.items.map(campo => {
                return {
                    id: campo.id,
                    campo: campo.label
                }
            })
        })
    }

    public camposPredetList() {
        this.api.getCamposPredet().subscribe(data => {
            // console.log(data);
            this.camposDet = data;
            let i = 0;
            const ArrayFields = [];
            console.log(this.camposDet)
            this.camposDet.forEach(campo => {
                i++;
                ArrayFields.push({
                    id: campo.id,
                    nombre: campo.label,
                    tipo: campo.formato,
                    requerido: true,
                    busqueda: true,
                    predeterminado: campo.default,
                    orden: i,
                    activo: 'A',
                    subcategorias: campo.subcategorias
                })
            })
            this.camposLista = this.orderFields(ArrayFields);
            this.dtTrigger.next();
            // console.log(this.camposLista)
        })
    }

    public verInfo() {
        this.campoView = this.camposTodos.filter(item => item.id === this.campoSelected.id)[0];
    }

    public saveList(template) {
        if (this.idList !== null && this.idList !== undefined && this.idList !== '') {

            if (this.descList !== null && this.descList !== undefined && this.descList !== '') {

                if (this.statusList !== undefined) {

                    if (this.extList !== undefined) {

                        if (this.addManager.length > 0) {

                            if (this.camposLista.length > 0) {

                                if (this.messageDft !== undefined && this.messageDft !== '' && this.arrayNivCrit.length > 0 ) {
                                    this.save()
                                } else {
                                    this.modalRef = this.modalService.show(template, {
                                        class: 'modal-primary',
                                        backdrop: true,
                                        ignoreBackdropClick: true,
                                        keyboard: false
                                    })
                                }
                            } else {
                                this.toastr.warningToastr('Debe agregar al menos un campo a la lista', this.TITLE);
                            }
                        } else {
                            this.toastr.warningToastr('Debe agregar al menos un responsable a la lista', this.TITLE);
                        }
                    } else {
                        this.toastr.warningToastr('Debe definir si la lista es pública o no', this.TITLE);
                    }
                } else {
                    this.toastr.warningToastr('Debe difinir el estado de la lista', this.TITLE);
                }
            } else {
                this.toastr.warningToastr('Debe agregar una descripción a la lista', this.TITLE);
            }
        } else {
            this.toastr.warningToastr('Debe asignar un id a la lista', this.TITLE);
        }
    }

    private saveModal() {
        this.modalRef.hide()
        this.save()
    }

    private save() {
        const nvCrit = {
            msgDft: this.messageDft,
            nivMsg: this.arrayNivCrit
        }
        const manager = []
        this.addManager.forEach(man => {
            manager.push(man.id);
        });
        const list = {
            nombre: this.idList,
            descripcion: this.descList,
            tipo: this.extList,
            estado: this.statusList,
            criticidad: JSON.stringify(nvCrit),
            responsables: manager
        }
        this.api.crearLista(list).subscribe(data => {
            console.log(data);

            switch (data.code) {
                case 0: {
                    const campos = [];
                    this.camposLista.forEach(campo => {
                        campos.push({
                            campoId: campo.id,
                            requerido: campo.requerido,
                            busqueda: campo.busqueda,
                            orden: campo.orden,
                            estado: campo.activo
                        })
                    });
                    const body = {
                        listaId: data.id,
                        campos: campos
                    }
                    this.api.asociarCampo(body).subscribe(camposL => {
                        console.log(camposL);
                        this.toastr.successToastr('La estructuta de la lista ha sido creada con éxito', this.TITLE);
                        this.router.navigate(['/app/admin/mantto-listas/list'])
                    });
                    break;
                }
                case 1: {
                    this.toastr.warningToastr('El valor idLista ya existe, favor cambiar el valor de idLista', this.TITLE);
                    break;
                }
                case 99: {
                    this.toastr.errorToastr('No se pudo registrar la lista', this.TITLE);
                    break;
                }
            }
        })
    }

    private addFieldBuildForm() {
        this.selectedScat = false
        this.arrayItemsScat = new Array<ISubCat>()
        this.addFieldForm = this.fb.group({
            nameField: ['', Validators.compose([Validators.required])],
            typeField: ['TEXT', Validators.compose([Validators.required])],
            requiredField: [true, Validators.compose([Validators.required])],
            searchField: [true, Validators.compose([Validators.required])],
            orderField: [1, Validators.compose([Validators.required])],
            statusField: [true, Validators.compose([Validators.required])],
            itemScat: ['']
        })
    }

    public addFL(field: ICampoDetalle) {


        if (field.requerido !== undefined &&
            field.busqueda !== undefined &&
            this.ordenCampoView !== undefined &&
            this.ordenCampoView !== null &&
            this.activeCampoView !== undefined ) {

            console.log(this.camposLista.filter( x => x.orden === this.ordenCampoView ))
            console.log(this.camposLista.find( x => x.orden === this.ordenCampoView ))

            if (this.camposLista.find( x => x.id === field.id ) === undefined) {
                if (this.camposLista.find( x => x.orden === this.ordenCampoView ) === undefined) {
                    this.camposLista.push({
                        id: field.id,
                        nombre: field.label,
                        tipo: field.formato,
                        requerido: this.stringToBool(field.requerido),
                        busqueda: this.stringToBool(field.busqueda),
                        predeterminado: field.predeterminado,
                        orden: this.ordenCampoView,
                        activo: this.activeCampoView,
                        subcategorias: field.subcategorias
                    })
                    this.campoSelected = [];
                    this.closeModal()
                    this.rerender()
                    this.initCampoView()
                } else {
                    this.toastr.warningToastr('El número de orden ya fue asignado', this.TITLE);
                }
            } else {
                this.toastr.warningToastr('El campo ya se encuentra asociado a la lista', this.TITLE);
            }
        } else {
            this.toastr.warningToastr('Complete todos los campos', this.TITLE);
        }
    }


    public addRepToList() {
        console.log(this.respSelected)
        if (this.respSelected !== null && this.respSelected !== undefined) {
            this.addManager.push({
                id: this.respSelected.id,
                label: this.respSelected.resp
            })
        } else {
            this.toastr.warningToastr('Ingrese un nombre para agregar a la lista', this.TITLE);
        }
    }

    public addMsgToList() {

        if (this.nivelNum > 0 && this.nivelMsg !== null && this.nivelMsg !== '' && this.nivelMsg !== undefined) {

            if (this.arrayNivCrit.find( item => item.id === this.nivelNum ) === undefined ) {
                this.arrayNivCrit.push({
                    id: this.nivelNum,
                    label: this.nivelMsg
                });
                this.nivelNum = 1
                this.nivelMsg = ''
            } else {
                this.toastr.warningToastr('El número asignado al mensaje de nivel ya fue agregado a la lista', this.TITLE);
            }
        } else {
            this.toastr.warningToastr('Ingrese un numero de nivel y un mensaje para agregar', this.TITLE);
        }
    }


    public deleteFieldToList(fieldRemove) {

        this.modalRef = this.modalService.show(ConfirmComponentComponent, Object.assign({
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        }))
        this.modalRef.content.onClose.subscribe(result => {
            if (result) {
                this.camposLista = this.camposLista.filter(function (field) {
                    return field !== fieldRemove
                })
                this.closeModal()
            } else {
                this.closeModal()
            }
        })
    }

    public deleteRep(repRemove: IManager) {
        this.addManager = this.addManager.filter(function (man) {
            return man !== repRemove
        })
    }

    public deleteNivCrit(nivRemove: ILabel) {
        this.arrayNivCrit = this.arrayNivCrit.filter(function (niv) {
            return niv !== nivRemove
        })
    }

    public addFieldList(template) {
        console.log(this.campoView);
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })

    }

    public editFieldList(template, ArrayEdit) {
        console.log(ArrayEdit);
        this.campoEdit.id = ArrayEdit.id
        this.campoEdit.label = ArrayEdit.nombre
        this.campoEdit.formato = ArrayEdit.tipo
        this.campoEdit.requerido = ArrayEdit.requerido
        this.campoEdit.busqueda = ArrayEdit.busqueda
        this.ordenCampoEdit = ArrayEdit.orden
        this.campoEdit.subcategorias = ArrayEdit.subcategorias
        this.activeCampoEdit = ArrayEdit.activo
        this.campoEdit.predeterminado = ArrayEdit.predeterminado

        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })

    }

    saveEditField() {

        if (this.campoEdit.requerido !== undefined &&
            this.campoEdit.busqueda !== undefined &&
            this.ordenCampoEdit !== undefined &&
            this.ordenCampoEdit !== null &&
            this.activeCampoEdit !== undefined ) {

            console.log(this.camposLista.filter(x => x.orden === this.ordenCampoEdit))
            console.log(this.camposLista.find(x => x.orden === this.ordenCampoEdit))

            if (this.camposLista.find(x => x.orden === this.ordenCampoEdit && x.id !== this.campoEdit.id) === undefined) {

                this.camposLista.forEach(campo => {
                    if (campo.id === this.campoEdit.id) {
                        campo.requerido = this.campoEdit.requerido,
                            campo.busqueda = this.campoEdit.busqueda,
                            campo.orden = this.ordenCampoEdit,
                            campo.activo = this.activeCampoEdit
                    }
                })
                this.camposLista = this.orderFields(this.camposLista);
                console.log(this.camposLista)
                this.modalRef.hide();
                this.rerender()

            } else {
                this.toastr.warningToastr('El número de orden ya fue asignado', this.TITLE);
            }
        } else {
            this.toastr.warningToastr('Complete todos los campos', this.TITLE);
        }

    }

    public closeModal() {
        this.modalRef.hide()
        this.addFieldBuildForm()
    }

    public saveCriticaly() {

        if (this.messageDft !== null && this.messageDft !== '' && this.messageDft !== undefined) {
            if (this.arrayNivCrit.length > 0) {
                this.toastr.successToastr('Configuración agregada con éxito', this.TITLE);
                this.modalRef.hide();
            } else {
                this.toastr.warningToastr('Debe asignar al menos un mensaje de nivel a la lista', this.TITLE);
            }
        } else {
            this.toastr.warningToastr('Debe asignar un mensaje general a la lista', this.TITLE);
        }
    }

    public closeModalAdd() {
        this.modalRef.hide();
        this.initCampoView();
        this.addFieldBuildForm();
        this.campoSelected = [];
    }

    public cancel() {
        this.router.navigate(['/app/admin/mantto-listas/list'])
    }

    public openCriticality(template) {
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public upFileList(template) {
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    private orderFields(arrayValues) {
        return arrayValues.sort((a, b) => {
            return a.orden - b.orden
        })
    }

    private stringToBool(value): boolean {

        switch (value) {
            case 'true':
                return true
            case true:
                return true
            case 'false':
                return false
            case false:
                return false
        }
    }

}
