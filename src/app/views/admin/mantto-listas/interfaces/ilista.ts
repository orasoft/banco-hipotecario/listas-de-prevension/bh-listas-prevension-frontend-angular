import {IcampoConfig} from './icampo-config';

export interface ILista {
    nombre: string,
    descripcion: string,
    tipo: string,
    estado: string,
    criticidad: string,
    responsables: [any],
    camposConfigurados?: [IcampoConfig],
    createdAt?: string,
    id?: string,
    updateAt?: string,
    version?: number
}
