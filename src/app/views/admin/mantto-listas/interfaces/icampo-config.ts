export interface IcampoConfig {
    busqueda: boolean
    campo: Campo,
    estado: string,
    id: string,
    orden: number,
    requerido: boolean
}

export interface Campo {
    createdAt: string,
    descripcion: string,
    formato: string,
    id: string,
    label: string,
    nombre: string,
    predeterminado: boolean,
    tipo: string,
    updatedAt: string,
    version: number,
    subcategorias: [any]
}
