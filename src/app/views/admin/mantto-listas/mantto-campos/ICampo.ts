export interface ICampos {
    total: number,
    items: [ICampoDetalle]
}

export interface ICampoDetalle {
    busqueda: boolean,
    createdAt: string,
    descripcion: string,
    formato: string,
    id: string,
    label: string,
    nombre: string,
    predeterminado: boolean,
    requerido: boolean,
    subcategorias: [ISubcat],
    tipo: string,
    updatedAt: string,
    version: number,
    default?: boolean
}

export interface ISubcat {
    createdAt: string,
    id: string,
    label: string,
    updatedAt: string,
    valor: string,
    version: number
}
