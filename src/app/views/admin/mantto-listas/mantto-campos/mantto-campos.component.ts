import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {IFileds, ISubCat} from '../../../../interface/ilist';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ApiService} from '../../../../services/apiService';
import {ICampoDetalle, ICampos} from './ICampo';
import {Subject} from 'rxjs/Subject';
import {DataTableDirective} from 'angular-datatables';
import {Router} from '@angular/router';


@Component({
    selector: 'app-mantto-campos',
    templateUrl: './mantto-campos.component.html'
})
export class ManttoCamposComponent implements OnInit, AfterViewInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtLanguage: DataTables.LanguageSettings = {
        search: 'Buscar',
        lengthMenu: 'Mostrar _MENU_ registros por página',
        info: 'Mostrando página _PAGE_ de _PAGES_',
        infoFiltered: '(Filtrado de _MAX_ registros)',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
        emptyTable: 'No se encontraron resultados'
    };

    dtTrigger: Subject<any> = new Subject();

    public TITLE = 'Mantenimiento de Campos'
    public modalRef: BsModalRef
    public addFieldForm: FormGroup
    public selectedScat = false
    public arrayItemsScat: Array<ISubCat> = new Array<ISubCat>()
    // public fields: Array<IFileds>  = new Array<IFileds>()
    public fields: [ICampoDetalle];
    public idCampoEdit: string;
    public profile

    constructor(private modalService: BsModalService,
                private fb: FormBuilder,
                public toastr: ToastrManager,
                public apiService: ApiService,
                public router: Router
    ) {
        this.profile = JSON.parse(localStorage.getItem('profile'))
        this.apiService.getAllCampos().subscribe(campos => {
            console.log(campos);
            this.fields = campos.items;
            this.rerender();
        })
    }

    ngOnInit(): void {
        this.dtOptions = {
            pagingType: 'full_numbers',
            language: this.dtLanguage
        };

        this.addFieldBuildForm();
    }

    ngAfterViewInit(): void {this.dtTrigger.next(); }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    public agregarCampos(template) {
        this.addFieldBuildForm();
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public closeModal() {
        this.modalRef.hide()
        this.addFieldBuildForm()
    }

    private addFieldBuildForm() {
        this.selectedScat = false
        this.arrayItemsScat = new Array<ISubCat>()
        this.addFieldForm = this.fb.group({
            nameField: ['', Validators.compose([Validators.required])],
            descField: ['', Validators.compose([Validators.required])],
            typeField: ['text', Validators.compose([Validators.required])],
            defaultField: [true, Validators.compose([Validators.required])],
            itemScat: ['']
        })
    }

    public addFieldToList() {

        if (this.addFieldForm.valid) {

            if (this.fields === undefined || this.fields.find( x => x.label.toLowerCase() === this.addFieldForm.getRawValue().nameField.toLowerCase() ) === undefined) {


                let type;
                if (this.addFieldForm.getRawValue().typeField.toLowerCase() === 'scat') {
                    type = 'selectbox'
                } else {
                    type = 'input'
                }

                const body = {
                    nombre: this.addFieldForm.getRawValue().nameField.replace(/ /g, '_').toLowerCase(),
                    label: this.addFieldForm.getRawValue().nameField,
                    predeterminado: this.stringToBool(this.addFieldForm.getRawValue().defaultField),
                    tipo: type,
                    formato: this.addFieldForm.getRawValue().typeField.toLowerCase(),
                    descripcion: this.addFieldForm.getRawValue().descField,
                    subcategorias: this.arrayItemsScat,
                    createdBy: this.profile.usuario.codbh
                }
                console.log(body)
                this.apiService.createCampo(body).subscribe(data => {
                    this.toastr.successToastr('Campo creado con éxito', this.TITLE)
                    this.modalRef.hide();
                    this.addFieldBuildForm();
                    this.apiService.getAllCampos().subscribe(campos => {
                        this.fields = campos.items;
                        this.rerender();
                    })
                }, error1 => {
                    this.toastr.errorToastr('No se pudo crear el campo', this.TITLE)
                })

            } else {
                this.toastr.warningToastr('El campo ya existe, debe nombrarlo diferente', this.TITLE);
            }

        } else {
            this.toastr.warningToastr('Favor completar toda la información solicitada', this.TITLE);
        }
    }

    public editToField() {

        if (this.addFieldForm.valid) {
            let type;
            if (this.addFieldForm.getRawValue().typeField.toLowerCase() === 'scat') {
                type = 'selectbox'
            } else {
                type = 'input'
            }
            const body = {
                id: this.idCampoEdit,
                nombre: this.addFieldForm.getRawValue().nameField.replace(/ /g, '_').toLowerCase(),
                predeterminado: this.stringToBool(this.addFieldForm.getRawValue().defaultField),
                tipo: type,
                formato: this.addFieldForm.getRawValue().typeField.toLowerCase(),
                descripcion: this.addFieldForm.getRawValue().descField,
                subcategorias: this.arrayItemsScat
            }
            console.log(body)
            this.apiService.updateCampo(this.idCampoEdit, body).subscribe( data => {
                this.toastr.successToastr('Campo actualizado con éxito', this.TITLE)
                this.modalRef.hide();
                this.apiService.getAllCampos().subscribe(campos => {
                    console.log(campos);
                    this.fields = campos.items;
                    this.rerender();
                })
            }, error1 => {
                this.toastr.errorToastr('No se pudo actualizar el campo', this.TITLE)
            })
        } else {
            this.toastr.warningToastr('Favor completar toda la información solicitada', this.TITLE);
        }
    }

    private stringToBool(value): boolean {

        switch (value) {
            case 'true':
                return true
            case true:
                return true
            case 'false':
                return false
            case false:
                return false
        }
    }

    public onChangeTypeField(value) {
        if (value === 'scat') {
            this.selectedScat = true
        } else {
            this.selectedScat = false
        }
    }

    public validateDupField(fieldNew: IFileds) {
        const value = false
        this.fields.forEach(field => {
            // if (field.label === fieldNew.label && field.type === fieldNew.type) {
            //     value = true
            // }
        })
        return value
    }

    public addScatToList() {
        if (this.addFieldForm.getRawValue().itemScat !== ''
            && this.addFieldForm.getRawValue().itemScat !== null
            && this.addFieldForm.getRawValue().itemScat !== undefined) {
            this.arrayItemsScat.push({
                label: this.addFieldForm.getRawValue().itemScat.toUpperCase(),
                valor: this.addFieldForm.getRawValue().itemScat,
            })
            this.addFieldForm.controls['itemScat'].setValue('')
        } else {
            this.toastr.warningToastr('Ingrese un item para agregar a la lista', this.TITLE);
        }
    }

    public deleteScat(scatRemove: ISubCat) {
        this.arrayItemsScat = this.arrayItemsScat.filter(function (scat) {
            return scat !== scatRemove
        })
    }

    public editFieldList(template, field) {

        this.idCampoEdit = field.id;
        this.addFieldForm.controls['nameField'].setValue(field.label)
        this.addFieldForm.controls['descField'].setValue(field.descripcion)
        this.addFieldForm.controls['typeField'].setValue(field.formato)
        this.addFieldForm.controls['defaultField'].setValue(field.predeterminado)

        if (field.formato === 'scat') {
            this.selectedScat = true;
            this.arrayItemsScat = field.subcategorias;
        }

        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public deleteFieldToList(field) {
    }

    public cancel() {
        this.router.navigate(['/app/admin/mantto-listas/list'])
    }
}
