import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ManttoListasEditarComponent} from './mantto-listas-editar.component';



describe('ManttoListasAgregarComponent', () => {
  let component: ManttoListasEditarComponent;
  let fixture: ComponentFixture<ManttoListasEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoListasEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoListasEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
