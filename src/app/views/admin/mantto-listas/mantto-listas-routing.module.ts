import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ManttoListasListComponent} from './mantto-listas-list/mantto-listas-list.component';
import {ManttoListasAgregarComponent} from './mantto-listas-agregar/mantto-listas-agregar.component';
import {ManttoCamposComponent} from './mantto-campos/mantto-campos.component';
import {ManttoListasEditarComponent} from './mantto-listas-editar/mantto-listas-editar.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ManttoListasListComponent,
        data: {
            title: 'Mantenimiento de listas - Listado'
        },
    },
    {
        path: 'agregar',
        component: ManttoListasAgregarComponent,
        data: {
            title: 'Mantenimiento de listas - Agregar lista'
        },
    },
    {
        path: 'editar',
        component: ManttoListasEditarComponent,
        data: {
            title: 'Mantenimiento de listas - Editar lista'
        },
    },
    {
        path: 'campos',
        component: ManttoCamposComponent,
        data: {
            title: 'Mantenimiento de campos - Agregar campos'
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ManttoListasRoutingModule {
}
