import {NgModule} from '@angular/core';
import {ManttoAreasListComponent} from './mantto-areas-list/mantto-areas-list.component';
import {ManttoAreasRoutingModule} from './mantto-areas-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {DataTablesModule} from 'angular-datatables';


@NgModule({
    imports: [
        ManttoAreasRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        DataTablesModule
    ],
    declarations: [ManttoAreasListComponent]
})

export class ManttoAreasModule {
}
