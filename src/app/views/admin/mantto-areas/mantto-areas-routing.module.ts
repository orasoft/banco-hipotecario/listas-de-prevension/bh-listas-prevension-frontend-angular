import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ManttoAreasListComponent} from './mantto-areas-list/mantto-areas-list.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ManttoAreasListComponent,
        data: {
            title: 'Mantenimiento de áreas - Listado'
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ManttoAreasRoutingModule {
}
