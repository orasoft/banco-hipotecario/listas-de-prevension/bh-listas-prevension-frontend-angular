import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoAreasListComponent } from './mantto-areas-list.component';

describe('ManttoAreasListComponent', () => {
  let component: ManttoAreasListComponent;
  let fixture: ComponentFixture<ManttoAreasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoAreasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoAreasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
