import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {NgOption} from '@ng-select/ng-select';
import {ToastrManager} from 'ng6-toastr-notifications';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-mantto-areas-list',
    templateUrl: './mantto-areas-list.component.html',
    styleUrls: ['./mantto-areas-list.component.scss']
})
export class ManttoAreasListComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtLanguage: DataTables.LanguageSettings = {
        search: 'Buscar',
        lengthMenu: 'Mostrar _MENU_ registros por página',
        info: 'Mostrando página _PAGE_ de _PAGES_',
        infoFiltered: '(Filtrado de _MAX_ registros)',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
        emptyTable: 'No se encontraron resultados'
    };

    dtTrigger: Subject<any> = new Subject();

    public TITLE = 'Mantenimiento de áreas'

    public modalRef: BsModalRef

    public areaNameValue: string
    public areaIdEditValue: number
    public areaNameEditValue: string
    public areaList = []
    public managerList = []
    public respSelected: NgOption;
    public serviceOptions: NgOption[];
    public areaSelected: number
    public userCatalog = []

    constructor(
        private router: Router,
        private modalService: BsModalService,
        private apiService: ApiService,
        public toastr: ToastrManager,
    ) {
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            language: this.dtLanguage,
            stateSave: false,
            ordering: false
        };
        this.respList()
        this.apiService.getAreas().subscribe(areas => {
            this.areaList = areas.items;
            this.dtTrigger.next();
        })
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    public respList() {
        this.apiService.getUserCatalog().subscribe(data => {
            this.userCatalog = data
            this.serviceOptions = data.map(resp => {
                return {
                    id: resp.id,
                    resp: resp.usuario
                }
            })
        })
    }

    public addRepToList() {
        console.log(this.respSelected)
        if (this.respSelected !== null && this.respSelected !== undefined) {

            const body = {
                areaId: this.areaSelected,
                responsableId: this.respSelected.id
            }

            this.apiService.regUserArea(body).subscribe(regUserArea => {
                this.toastr.successToastr('Responsable agregado con éxito', this.TITLE);
                this.apiService.getAreasId(this.areaSelected).subscribe(userAreaList => {
                    const values = []
                    this.respSelected = []
                    userAreaList.responsables.forEach(item => {
                        values.push({
                            id: item.id,
                            label: item.usuario
                        })
                    })

                    this.managerList = values
                    console.log(this.managerList);
                })
            }, error1 => {
                this.toastr.errorToastr('No se pudo agregar el responsable', this.TITLE);
            })
        } else {
            this.toastr.warningToastr('Ingrese un nombre para agregar a la lista', this.TITLE);
        }
    }

    public cancel() {
        this.router.navigate(['/app/home'])
    }

    public addRespList(template, idArea) {
        this.respList();
        this.areaSelected = idArea;
        this.apiService.getAreasId(this.areaSelected).subscribe(userAreaList => {
            const values = []
            userAreaList.responsables.forEach(item => {
                values.push({
                    id: item.id,
                    label: item.usuario
                })
            })

            this.managerList = values
            console.log(this.managerList);
        })
        this.modalRef = this.modalService.show(template, {
            class: 'modal-lg modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public addArea(template) {
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public editArea(template, id, nombre) {
        this.areaIdEditValue = id;
        this.areaNameEditValue = nombre;
        console.log(this.areaNameEditValue)
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public execUpdate() {
        const body = {
            id: this.areaIdEditValue,
            nombre: this.areaNameEditValue
        }
        this.apiService.updateArea(body).subscribe(data => {
            this.toastr.successToastr('Area actualizada con éxito', 'Mantenimiento de Areas')
            this.modalRef.hide()
            this.apiService.getAreas().subscribe(areas => {
                this.areaList = areas.items;
                this.rerender();
            })
        }, error1 => {
            this.toastr.errorToastr('No se pudo actualizar el Area', 'Mantenimiento de Areas')
        })
    }

    public removeUser(id) {
        const body = {
            areaId: this.areaSelected,
            responsableId: id
        }
        console.log(body);
        this.apiService.removeUserArea(body).subscribe(data => {
            this.toastr.successToastr('Responsable eliminado con éxito', 'Mantenimiento de Areas')
            this.apiService.getAreasId(this.areaSelected).subscribe(userAreaList => {
                const values = []
                userAreaList.responsables.forEach(item => {
                    values.push({
                        id: item.id,
                        label: item.usuario
                    })
                })

                this.managerList = values
                console.log(this.managerList);
            })
        }, error1 => {
            this.toastr.errorToastr('No se pudo eliminar el responsable', 'Mantenimiento de Areas')
        })
    }

    public registerArea() {
        const body = {
            nombre: this.areaNameValue
        }
        this.apiService.crearArea(body).subscribe(data => {
            this.toastr.successToastr('Area creada con éxito', 'Mantenimiento de Areas')
            this.modalRef.hide()
            this.areaNameValue = '';
            this.apiService.getAreas().subscribe(areas => {
                this.areaList = []
                this.areaList = areas.items;
                this.areaNameValue = ''
                this.rerender()
            })
        }, error1 => {
            this.toastr.errorToastr('No se pudo crear el Area', 'Mantenimiento de Areas')
        })
    }

    public closeModal() {
        this.modalRef.hide();
        this.areaNameValue = ''
        this.respSelected = []
    }

    public exit() {
        this.modalRef.hide()
        this.respSelected = []
    }

    public findName(id) {
        return this.userCatalog.filter(item => Number(item.id) === id)[0].usuario
    }

    public removeArea(id) {
        this.apiService.removeArea(id).subscribe(data => {
            this.toastr.successToastr('Area eliminada con éxito', 'Mantenimiento de Areas')
            this.apiService.getAreas().subscribe(areas => {
                this.areaList = areas.items;
                this.rerender();
            })
        }, error1 => {
            this.toastr.errorToastr('No se pudo eliminar el area', 'Mantenimiento de Areas')
        })
    }

}
