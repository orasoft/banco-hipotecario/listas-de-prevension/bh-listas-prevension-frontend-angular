import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {FormGroup} from '@angular/forms';
import {IRegisterList} from '../../../../interface/iRegisterList';
import {IFileds, IList} from '../../../../interface/ilist';
import {ApiService} from '../../../../services/apiService';
import {ParseFieldsService} from '../../../../services/parseFieldsService';
import {ExportAsConfig, ExportAsService, SupportedExtensions} from 'ngx-export-as';
import {IPermPerfil} from '../../../../interface/iperm-perfil';
import {PaginationInstance} from 'ngx-pagination';
import {IRoleSelected} from '../../../../interface/irole-selected';
import {ExcelService} from '../../../../services/excelService';
import {ToastrManager} from 'ng6-toastr-notifications';
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-consulta-individual-detail',
  templateUrl: './consulta-individual-detail.component.html',
  styleUrls: ['./consulta-individual-detail.component.scss']
})
export class ConsultaIndividualDetailComponent implements OnInit {

  config: ExportAsConfig = {
    type: 'pdf',
    elementId: 'tableToContent',
    options: {
      jsPDF: {
        orientation: 'portrait'
      }
    }
  };

  public TITLE = 'Consulta Individual de Personas'

  public modalRef: BsModalRef

  public listaList = []
  public valueList
  public fields: FieldBase<any>[]
  public listSearchForm: FormGroup
  public camposHeaders = []
  public fieldsRegister = []
  public permPerfil: IPermPerfil
  public p = 1;

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  }
  public filter = '';
  public roleSelected: IRoleSelected

  constructor(private router: Router,
              private modalService: BsModalService,
              private apiService: ApiService,
              private parse: ParseFieldsService,
              private excelService: ExcelService,
              private exportAsService: ExportAsService,
              private toastr: ToastrManager) {
    this.roleSelected = JSON.parse(localStorage.getItem('role'))
  }

  ngOnInit() {

    this.fields = []
    this.onChange()
  }

  onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  private getList() {
    this.apiService.todasListas().subscribe(data => {
      this.listaList = data.items;
    })
  }

  public exportAs(type: SupportedExtensions, opt?: string) {
    this.config.type = type;
    if (opt) {
      this.config.options.jsPDF.orientation = opt;
    }
    this.exportAsService.save(this.config, 'export_' + Date.now()).subscribe(() => {
    });
  }

  public closeModal() {
    this.modalRef.hide()
  }

  private getPermisos() {

    this.apiService.getRolesList().subscribe( data => {
      const role = data.filter( item => Number(item.idBhRol) === Number(this.roleSelected.id))[0]
      this.apiService.getPermPerfil(`?rolId=${role.id}&listaId=*`).subscribe(perm => {
        this.permPerfil = perm[0]
        // console.log(this.permPerfil)
      })
    })
  }

  public onChange() {
    this.fields = [];
    this.camposHeaders = []
    this.fieldsRegister = []
      this.getPermisos()
      this.apiService.getCamposPredet().subscribe(campos => {
        campos.forEach(campo => {
          this.fields.push({
            name: campo.nombre,
            index: campo.id,
            label: campo.label,
            type: campo.formato,
            required: true,
            search: true,
            order: campo.orden,
            status: campo.estado,
            subcategories: campo.subcategorias
          })
        })
        this.fields = this.parse.setFieldsForm(this.fieldSearch(this.fields))
        this.listSearchForm = this.parse.toFormGroup(this.fields)
      })
  }

  private ordenLista(list) {
    return list.sort((a, b) => {
      return a.orden - b.orden
    })
  }

  public fieldSearch(data) {
    const fl: IFileds[] = []
    data.forEach(field => {
      if (field.search === true) {
        fl.push(field)
      }
    })
    return fl
  }

  private orderList(list) {
    return list.sort((a, b) => {
      return a.order - b.order
    })
  }

  public editar(id, reg) {
    this.router.navigate(['/app/upload/mantto-individual/create', {create: false, idList: id, reg: reg}])
  }

  public filterIdList() {
      const filters = [];
      this.camposHeaders = []
      this.fieldsRegister = []
      const listFormKeys = Object.keys(this.listSearchForm.controls);
      listFormKeys.forEach(fieldForm => {
        const field = this.fields.filter(item => item.index === fieldForm)[0];

        if (this.listSearchForm.controls[fieldForm].value !== undefined &&
            this.listSearchForm.controls[fieldForm].value !== null &&
            this.listSearchForm.controls[fieldForm].value.trim() !== '') {

          filters.push({
            campoId: field.index,
            valor: this.listSearchForm.controls[fieldForm].value
          })
        }
      })

      if (filters.length > 0) {

        const params = {
          perfilId: '*',
          filtros: filters
        }
        this.apiService.getRegistros(params).subscribe( data => {

          this.camposHeaders =  this.ordenLista(data.header);
          console.log(this.camposHeaders)
          this.fieldsRegister = []
          data.filas.forEach( linea => {
            const linesReg = []
            linea.campos.forEach( campo => {

              if (this.camposHeaders.filter(item => item.campoId === campo.id).length > 0) {
                const values = this.camposHeaders.filter(item => item.campoId === campo.id)[0]
                linesReg.push({
                  busqueda: linea.busquedaExacta,
                  lista: linea.lista,
                  listaId: linea.listaId,
                  responsable: linea.responsable,
                  orden: values.orden,
                  data: {
                    id: campo.id,
                    valor: campo.valor,
                    idtrx: linea.id
                  }
                })
              }
            })
            if (linea.busquedaExacta) {
              linesReg.push({
                data: {
                  valor: 'Exacto'
                }
              })
            } else {
              linesReg.push({
                data: {
                  valor: 'Homónimo'
                }
              })
            }
            if (linea.tipo === 'I') {
              linesReg.push({
                data: {
                  valor: 'Individual'
                }
              })
            } else {
              linesReg.push({
                data: {
                  valor: 'Masivo'
                }
              })
            }
            linesReg.push({
              data: {
                valor: linea.responsable
              }
            })
            linesReg.push({
              data: {
                valor: linea.lista
              }
            })
            this.fieldsRegister.push(linesReg);
          })
        })
      } else {
        this.toastr.warningToastr('Debe de ingresar al menos un valor de búsqueda', this.TITLE)
      }
  }

  public exportExcel() {

    const data = [];

    // console.log(this.camposHeaders)
    // console.log(this.fieldsRegister)
    // this.camposHeaders.push({
    //   label: 'Exacto/Homonimo'
    // })
    // this.camposHeaders.push({
    //   label: 'Masivo/Individual'
    // })
    // this.camposHeaders.push({
    //   label: 'Responsable'
    // })
    // this.camposHeaders.push({
    //   label: 'Lista'
    // })
    // console.log(this.fieldsRegister)
    this.fieldsRegister.forEach(reg => {
      const value = {}
      for (let i = 0; i < this.camposHeaders.length; i++) {
        value[this.camposHeaders[i].label] = reg[i].data.valor
      }
      data.push(value)
    })
    this.excelService.exportAsExcelFile(data, 'consulta');
  }

  generatePDF() {
    const doc = new jsPDF('l', 'mm', 'a4');
    doc.setFontSize(12)
    doc.text('Sistema Manejo de Listas de Prevención', 10, 15);
    doc.text('Listado de registros (Consulta Individual de Personas)', 10, 20);
    // doc.text(50, 100, 'page 1')
    // doc.addPage();
    // doc.text(50, 100, 'page 2')
    html2canvas( document.getElementById('tableToContent')).then(function(canvas) {
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;
      const position = 25;
      const img = canvas.toDataURL('image/png');
      doc.addImage(img, 'PNG', 10, position, imgWidth, imgHeight);
      doc.save('export_' + Date.now() + '.pdf');
    });
  }

}
