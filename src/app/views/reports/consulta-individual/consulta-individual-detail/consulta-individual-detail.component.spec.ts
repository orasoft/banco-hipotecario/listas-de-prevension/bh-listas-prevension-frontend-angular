import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaIndividualDetailComponent } from './consulta-individual-detail.component';

describe('ConsultaIndividualDetailComponent', () => {
  let component: ConsultaIndividualDetailComponent;
  let fixture: ComponentFixture<ConsultaIndividualDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaIndividualDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaIndividualDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
