import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConsultaIndividualDetailComponent} from './consulta-individual-detail/consulta-individual-detail.component';


export const routes: Routes = [
    {
        path: 'list',
        component: ConsultaIndividualDetailComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ConsultaIndividualRoutingModule {
}
