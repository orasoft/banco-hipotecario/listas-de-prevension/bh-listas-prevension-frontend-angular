import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


export const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'consulta-historica',
                loadChildren: './consulta-historica/consulta-historica.module#ConsultaHistoricaModule',
            },
            {
                path: 'consulta-individual',
                loadChildren: './consulta-individual/consulta-individual.module#ConsultaIndividualModule',
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ReportsRoutingModule {
}
