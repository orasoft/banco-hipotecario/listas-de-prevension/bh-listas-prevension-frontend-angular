import {NgModule} from '@angular/core';
import {ConsultaHistoricaListComponent} from './consulta-historica-list/consulta-historica-list.component';
import {ConsultaHistoricaRoutingModule} from './consulta-historica-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {DataTablesModule} from 'angular-datatables';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
    imports: [
        ConsultaHistoricaRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DataTablesModule,
        NgxPaginationModule
    ],
    declarations: [ConsultaHistoricaListComponent]
})

export class ConsultaHistoricaModule {
}
