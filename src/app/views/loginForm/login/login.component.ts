import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {AuthService} from '../../../services/authService';
import {Usuario} from '../usuario';
import {IMenu} from '../../../interface/imenu';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup

  constructor(
      private fb: FormBuilder,
      private router: Router,
      public toastr: ToastrManager,
      private auth: AuthService
  ) { }

  ngOnInit() {
    this.builForm();
  }

  private builForm() {
    this.loginForm = this.fb.group( {
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  login() {
    // this.router.navigate(['/app/home']);

    if (this.loginForm.valid) {

      const user: Usuario = {
        username: this.loginForm.getRawValue().username,
        password: this.loginForm.getRawValue().password
      }
      // this.router.navigate(['/app/home']);
      // this.toastr.successToastr('Bienvenido al sistema', 'Inicio de Sesión');
      this.auth.login(user).subscribe(  resp => {
        console.log(resp)
          if (resp.accessToken) {
              localStorage.setItem('token', resp.accessToken);
              this.auth.getRoles().subscribe( profile => {
                  localStorage.setItem('profile', JSON.stringify(profile));
                  this.router.navigate(['/login/roles'])
              });
          } else if (resp.status) {
            if (resp.status === 401) {
              this.toastr.warningToastr('Usuario/Clave incorrectos', 'Inicio de Sesión');
            } else {
              this.toastr.errorToastr('Error de comuniciación', 'Inicio de Sesión');
            }
          } else {
            this.toastr.errorToastr('Error de comuniciación', 'Inicio de Sesión');
          }
      }, error1 => {
        console.log(error1)
        console.log(error1.status)
        if ( error1.status === 400 ) {
          this.toastr.warningToastr('Credenciales incorrctas', 'Inicio de Sesión');
        } else {
          this.toastr.errorToastr('Error de comuniciación', 'Inicio de Sesión');
        }
      })

    } else {
      this.toastr.warningToastr('Ingrese usuario y contraseña correctamente', 'Inicio de Sesión');
    }
  }

  // login () {
  //   // tslint:disable-next-line:max-line-length
  //  const menu = JSON.parse('{"user":{"id":"8fe71887-6701-40b7-9913-5b286d1392bc","codbh":"00000","usuario":"Pruebas Aplicativos","createdAt":"2019-07-25T21:43:17.048Z","updatedAt":"2019-07-25T21:43:17.048Z","version":1},"roles":[{"id":"24","rol":"Administrador"}],"accesos":[{"name":"Administrador","id":"24","access":[{"id":["196"],"type":"DIRECTO","name":["Inicio "],"url":["/app/home"],"icon":["fa fa-home"]},{"id":["197"],"type":"MENU","name":["Administracion"],"icon":["fa fa-folder"]},{"id":["201"],"type":"MENU","name":["Carga Informacion"],"icon":["fa fa-upload"]},{"id":["204"],"type":"MENU","name":["Consultas y Reportes"],"icon":["fa fa-file-text-o"]},{"id":["200"],"idPrincipal":["197"],"type":"DIRECTO","name":["Mantenimiento Areas"],"url":["/app/admin/mantto-areas/list"],"icon":["fa fa-th-large"]},{"id":["208"],"idPrincipal":["197"],"type":"DIRECTO","name":["Conexion a Listas Externas"],"url":["/app/admin/conex-list/conex-list-ext"],"icon":["fa fa-plug"]},{"id":["202"],"idPrincipal":["201"],"type":"DIRECTO","name":["Carga Masiva Listas"],"url":["/app/upload/carga-masiva/list"],"icon":["fa fa-angle-double-up"]},{"id":["203"],"idPrincipal":["201"],"type":"DIRECTO","name":["Mantenimiento Individual Listas"],"url":["/app/upload/mantto-individual/list"],"icon":["fa fa-angle-up"]},{"id":["206"],"idPrincipal":["204"],"type":"DIRECTO","name":["Consulta Individual Persona"],"url":["/app/reports/consulta-individual/list"],"icon":["fa fa-user"]},{"id":["205"],"idPrincipal":["204"],"type":"DIRECTO","name":["Consulta Historica Informacion Listas"],"url":["/app/reportes/consulta-historica/list"],"icon":["fa fa-file-text"]},{"id":["199"],"idPrincipal":["197"],"type":"DIRECTO","name":["Asignacion Perfiles"],"url":["/app/admin/asig-perfiles/list"],"icon":["fa fa-id-card-o"]},{"id":["198"],"idPrincipal":["197"],"type":"DIRECTO","name":["Mantenimiento Listas"],"url":["/app/admin/mantto-listas/list"],"icon":["fa fa-bars"]}]}]}')
  //   const userMenuM = []
  //   const userMenuD = []
  //   menu.accesos.forEach( acc => {
  //     if ( acc.id === '24' ) {
  //       acc.access.forEach( opMenu => {
  //         console.log(opMenu.type)
  //
  //         if ( opMenu.type === 'MENU' ) {
  //           userMenuM.push({
  //             id: opMenu.id[0],
  //             idPrincipal: opMenu.idPrincipal !== undefined ? opMenu.idPrincipal[0] : undefined,
  //             name: opMenu.name[0],
  //             type: opMenu.type,
  //             url: opMenu.url !== undefined ? opMenu.url[0] : undefined,
  //             icon: opMenu.icon[0]
  //           })
  //         } else if (opMenu.type === 'DIRECTO') {
  //           userMenuD.push({
  //             id: opMenu.id[0],
  //             idPrincipal: opMenu.idPrincipal !== undefined ? opMenu.idPrincipal[0] : undefined,
  //             name: opMenu.name[0],
  //             type: opMenu.type,
  //             url: opMenu.url !== undefined ? opMenu.url[0] : undefined,
  //             icon: opMenu.icon[0]
  //           })
  //         }
  //       })
  //     }
  //   })
  //   const navigation = []
  //   navigation.push({
  //         title: true,
  //         name: 'Menu Principal'
  //       })
  //   navigation.push(
  //       {
  //         name: 'Inicio',
  //         url: '/app/home',
  //         icon: 'fa fa-home'
  //       })
  //   userMenuM.forEach( opMenu => {
  //     const child = []
  //     userMenuD.forEach( opSubM => {
  //       if ( opSubM.idPrincipal === opMenu.id ) {
  //         child.push({
  //           name: opSubM.name,
  //           url: opSubM.url,
  //           icon: opSubM.icon
  //         })
  //       }
  //     })
  //     navigation.push( {
  //       name: opMenu.name,
  //       url: opMenu.url,
  //       icon: opMenu.icon,
  //       children: child
  //     })
  //   })
  //   console.log(navigation)
  // }
}
