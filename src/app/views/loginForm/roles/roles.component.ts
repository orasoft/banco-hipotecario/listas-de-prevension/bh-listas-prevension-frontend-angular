import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

    private profile;
    public roles = [];

    constructor(
        private router: Router,
        public toastr: ToastrManager,
    ) {
        this.profile = JSON.parse(localStorage.getItem('profile'));
        console.log(this.profile);
        this.roles = this.profile.roles;
    }

    ngOnInit() {
        if (this.roles.length > 0) {
            localStorage.setItem('role', JSON.stringify(this.roles[0]));
            this.router.navigate(['/app/home']);
            this.toastr.successToastr('Bienvenido al sistema', 'Inicio de Sesión');
        }
    }

    public selectRole(rol) {
        localStorage.setItem('role', JSON.stringify(rol));
        this.router.navigate(['/app/home']);
        this.toastr.successToastr('Bienvenido al sistema', 'Inicio de Sesión');
    }
}
