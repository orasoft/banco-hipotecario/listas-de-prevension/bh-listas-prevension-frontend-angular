import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


export const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'carga-masiva',
                loadChildren: './carga-masiva/carga-masiva.module#CargaMasivaModule'
            },
            {
                path: 'mantto-individual',
                loadChildren: './mantto-individual/mantto-individual.module#ManttoIndividualModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class UploadRoutingModule {
}
