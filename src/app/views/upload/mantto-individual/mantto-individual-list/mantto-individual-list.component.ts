import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {IFileds, IList} from '../../../../interface/ilist';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {ParseFieldsService} from '../../../../services/parseFieldsService';
import {FormGroup} from '@angular/forms';
import {ILista} from '../../../admin/mantto-listas/interfaces/ilista';
import {IPermPerfil} from '../../../../interface/iperm-perfil';
import {ToastrManager} from 'ng6-toastr-notifications';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {IRoleSelected} from '../../../../interface/irole-selected';
import {PaginationInstance} from 'ngx-pagination';

@Component({
    selector: 'app-mantto-individual-list',
    templateUrl: './mantto-individual-list.component.html',
    styleUrls: ['./mantto-individual-list.component.scss']
})
export class ManttoIndividualListComponent implements OnInit, AfterViewInit {

    // @ts-ignore
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtLanguage: DataTables.LanguageSettings = {
        search: 'Buscar',
        lengthMenu: 'Mostrar _MENU_ registros por página',
        info: 'Mostrando página _PAGE_ de _PAGES_',
        infoFiltered: '(Filtrado de _MAX_ registros)',
        paginate: {
            first: 'Primero',
            last: 'Último',
            next: 'Siguiente',
            previous: 'Anterior'
        },
        emptyTable: 'No se encontraron resultados'
    };

    dtTrigger: Subject<any> = new Subject();

    public TITLE = 'Mantenimiento individual de Listas'
    public modalRef: BsModalRef

    // public listaList: IListaCatalog[]
    public listaList = []
    public valueList
    public fields: FieldBase<any>[]
    public listSearchForm: FormGroup

    // public fieldsRegister: IRegisterList[]
    public fieldsRegister = []
    public listaPP: ILista
    public fNameAll;
    public fNacAll;
    public fFechaAll;
    public fTdocAll;
    public fDocAll;

    public permPerfil: IPermPerfil
    public camposHeaders = []

    public p = 1;
    public roleSelected: IRoleSelected

    public configPage: PaginationInstance = {
        id: 'advanced',
        itemsPerPage: 10,
        currentPage: 1
    }
    public idRegistroAnular: string
    public motivo: string
    public viewResp = false;

    constructor(
        private router: Router,
        private modalService: BsModalService,
        private apiService: ApiService,
        private parse: ParseFieldsService,
        private toastr: ToastrManager
    ) {
        this.roleSelected = JSON.parse(localStorage.getItem('role'))
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            language: this.dtLanguage,
            ordering: false
        };
        this.fields = []
        this.getList()
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    onPageChange(number: number) {
        this.configPage.currentPage = number;
    }

    private getPermisos() {
        this.viewResp = false;
        this.apiService.getRolesList().subscribe(data => {
            const role = data.filter(item => Number(item.idBhRol) === Number(this.roleSelected.id))[0]
            this.apiService.getPermPerfil(`?rolId=${role.id}&listaId=${this.valueList}`).subscribe(perm => {
                this.permPerfil = perm[0]
                if (this.permPerfil !== undefined) {
                    if (this.permPerfil.responsable) {
                        this.viewResp = this.permPerfil.responsable;
                    }
                }
                console.log(this.permPerfil)
            })
        })
    }

    private getList() {
        this.apiService.todasListas().subscribe(data => {
            this.listaList = data.items;
        })
    }

    public createPeople() {

        // if (this.permPerfil !== undefined) {
        //     if (this.permPerfil.acceso === 'Edicion') {
        this.router.navigate(['/app/upload/mantto-individual/load'])
        // } else {
        //     this.toastr.warningToastr('Su rol no tiene permisos de edición para esta lista', this.TITLE)
        // }
        // } else {
        //     this.toastr.warningToastr('Su rol no tiene permisos asignados', this.TITLE)
        // }
    }

    public editPeople() {
        this.router.navigate(['/app/upload/mantto-individual/edit'])
    }

    public closeModal() {
        this.modalRef.hide()
    }

    public onChange() {
        this.fields = [];
        this.camposHeaders = []
        this.fieldsRegister = []
        if (this.valueList !== '*') {
            this.getPermisos()

            this.apiService.getListaCampos(this.valueList).subscribe(data => {
                data.camposConfigurados.forEach(campo => {
                    this.fields.push({
                        name: campo.campo.nombre,
                        index: campo.campo.id,
                        label: campo.campo.label,
                        type: campo.campo.formato,
                        required: campo.requerido,
                        search: campo.busqueda,
                        order: campo.orden,
                        status: campo.estado,
                        subcategories: campo.campo.subcategorias
                    })
                })
                this.fields = this.parse.setFieldsForm(this.fieldSearch(this.fields))
                this.fields = this.orderList(this.fields)
                this.listSearchForm = this.parse.toFormGroup(this.fields)
            })
        } else {
            this.getPermisos()
            this.apiService.getCamposPredet().subscribe(campos => {
                campos.forEach(campo => {
                    this.fields.push({
                        name: campo.nombre,
                        index: campo.id,
                        label: campo.label,
                        type: campo.formato,
                        required: true,
                        search: true,
                        order: campo.orden,
                        status: campo.estado,
                        subcategories: campo.subcategorias
                    })
                })
                this.fields = this.parse.setFieldsForm(this.fieldSearch(this.fields))
                this.listSearchForm = this.parse.toFormGroup(this.fields)
            })
        }
    }

    public fieldSearch(data) {
        const fl: IFileds[] = []
        data.forEach(field => {
            if (field.search === true) {
                fl.push(field)
            }
        })
        return fl
    }

    private orderList(list) {
        return list.sort((a, b) => {
            return a.order - b.order
        })
    }

    private ordenLista(list) {
        return list.sort((a, b) => {
            return a.orden - b.orden
        })
    }

    public editar(info) {
        console.log(info);
        if (this.permPerfil !== undefined) {
            if (this.permPerfil.acceso === 'Edicion') {
                this.router.navigate(['/app/upload/mantto-individual/edit', {idList: info[0].listaId, idTrx: info[0].data.idtrx}])
            } else {
                this.toastr.warningToastr('Su rol no tiene permisos de edición para esta lista', this.TITLE)
            }
        } else {
            this.toastr.warningToastr('Su rol no tiene permisos asignados', this.TITLE)
        }
    }

    public modalAnulation(template, id) {

        if (this.permPerfil !== undefined) {
            if (this.permPerfil.acceso === 'Edicion') {
                this.idRegistroAnular = id;
                console.log(this.idRegistroAnular)
                this.modalRef = this.modalService.show(template, {
                    class: 'modal-primary',
                    backdrop: true,
                    ignoreBackdropClick: true,
                    keyboard: false
                })
            } else {
                this.toastr.warningToastr('Su rol no tiene permisos de edición para esta lista', this.TITLE)
            }
        } else {
            this.toastr.warningToastr('Su rol no tiene permisos asignados', this.TITLE)
        }
    }

    public darBajaRegistro() {
        const body = {
            justificacion: this.motivo
        }
        this.apiService.anularRegistro(this.idRegistroAnular, body).subscribe(data => {
            this.modalRef.hide()
            this.toastr.successToastr('Baja de registro efectuada con éxito', this.TITLE)
        })
    }

    public filterIdList() {

        if (this.permPerfil !== undefined) {

            if (this.permPerfil.acceso === 'Consulta' || this.permPerfil.acceso === 'Edicion') {
                const filters = [];
                this.camposHeaders = []
                this.fieldsRegister = []
                const listFormKeys = Object.keys(this.listSearchForm.controls);
                listFormKeys.forEach(fieldForm => {
                    const field = this.fields.filter(item => item.index === fieldForm)[0];

                    if (this.listSearchForm.controls[fieldForm].value !== undefined &&
                        this.listSearchForm.controls[fieldForm].value !== null &&
                        this.listSearchForm.controls[fieldForm].value.trim() !== '') {

                        filters.push({
                            campoId: field.index,
                            valor: this.listSearchForm.controls[fieldForm].value
                        })
                    }
                })

                if (filters.length > 0) {
                    let perf = ''
                    if (this.valueList !== '*') {
                        perf = this.permPerfil.id
                    } else {
                        perf = this.valueList
                    }

                    const params = {
                        perfilId: perf,
                        filtros: filters
                    }
                    this.apiService.getRegistros(params).subscribe(data => {

                        this.camposHeaders = this.ordenLista(data.header);
                        console.log(this.camposHeaders)
                        this.fieldsRegister = []
                        data.filas.forEach(linea => {
                            const linesReg = []
                            linea.campos.forEach(campo => {

                                if (this.camposHeaders.filter(item => item.campoId === campo.id).length > 0) {
                                    const values = this.camposHeaders.filter(item => item.campoId === campo.id)[0]
                                    linesReg.push({
                                        busqueda: linea.busquedaExacta,
                                        lista: linea.lista,
                                        listaId: linea.listaId,
                                        responsable: linea.responsable,
                                        orden: values.orden,
                                        data: {
                                            id: campo.id,
                                            valor: campo.valor,
                                            idtrx: linea.id
                                        }
                                    })
                                }
                            })
                            if (linea.busquedaExacta) {
                                linesReg.push({
                                    data: {
                                        valor: 'Exacto'
                                    }
                                })
                            } else {
                                linesReg.push({
                                    data: {
                                        valor: 'Homónimo'
                                    }
                                })
                            }
                            if (linea.tipo === 'I') {
                                linesReg.push({
                                    data: {
                                        valor: 'Individual'
                                    }
                                })
                            } else {
                                linesReg.push({
                                    data: {
                                        valor: 'Masivo'
                                    }
                                })
                            }
                            if (this.viewResp) {
                                linesReg.push({
                                    data: {
                                        valor: linea.responsable
                                    }
                                })
                            }
                            linesReg.push({
                                data: {
                                    valor: linea.lista
                                }
                            })
                            this.fieldsRegister.push(linesReg);
                        })
                        console.log(this.camposHeaders)
                        console.log(this.fieldsRegister)
                    })
                } else {
                    this.toastr.warningToastr('Debe de ingresar al menos un valor de búsqueda', this.TITLE)
                }

            } else {
                this.toastr.warningToastr('No tiene permisos para consultar sobre la lista', this.TITLE)
            }

        } else if (this.permPerfil === undefined && this.valueList === '*') {

            const filters = [];
            this.camposHeaders = []
            this.fieldsRegister = []
            const listFormKeys = Object.keys(this.listSearchForm.controls);
            listFormKeys.forEach(fieldForm => {
                const field = this.fields.filter(item => item.index === fieldForm)[0];

                if (this.listSearchForm.controls[fieldForm].value !== undefined &&
                    this.listSearchForm.controls[fieldForm].value !== null &&
                    this.listSearchForm.controls[fieldForm].value.trim() !== '') {

                    filters.push({
                        campoId: field.index,
                        valor: this.listSearchForm.controls[fieldForm].value
                    })
                }
            })

            if (filters.length > 0) {
                let perf = ''
                if (this.valueList !== '*') {
                    perf = this.permPerfil.id
                } else {
                    perf = this.valueList
                }

                const params = {
                    perfilId: perf,
                    filtros: filters
                }
                this.apiService.getRegistros(params).subscribe(data => {

                    this.camposHeaders = this.ordenLista(data.header);
                    console.log(this.camposHeaders)
                    this.fieldsRegister = []
                    data.filas.forEach(linea => {
                        const linesReg = []
                        linea.campos.forEach(campo => {

                            if (this.camposHeaders.filter(item => item.campoId === campo.id).length > 0) {
                                const values = this.camposHeaders.filter(item => item.campoId === campo.id)[0]
                                linesReg.push({
                                    busqueda: linea.busquedaExacta,
                                    lista: linea.lista,
                                    listaId: linea.listaId,
                                    responsable: linea.responsable,
                                    orden: values.orden,
                                    data: {
                                        id: campo.id,
                                        valor: campo.valor,
                                        idtrx: linea.id
                                    }
                                })
                            }
                        })
                        if (linea.busquedaExacta) {
                            linesReg.push({
                                data: {
                                    valor: 'Exacto'
                                }
                            })
                        } else {
                            linesReg.push({
                                data: {
                                    valor: 'Homónimo'
                                }
                            })
                        }
                        if (linea.tipo === 'I') {
                            linesReg.push({
                                data: {
                                    valor: 'Individual'
                                }
                            })
                        } else {
                            linesReg.push({
                                data: {
                                    valor: 'Masivo'
                                }
                            })
                        }
                        if (this.viewResp) {
                            linesReg.push({
                                data: {
                                    valor: linea.responsable
                                }
                            })
                        }
                        linesReg.push({
                            data: {
                                valor: linea.lista
                            }
                        })
                        this.fieldsRegister.push(linesReg);
                        console.log(this.fieldsRegister)
                    })
                })
            } else {
                this.toastr.warningToastr('Debe de ingresar al menos un valor de búsqueda', this.TITLE)
            }


        } else {
            this.toastr.warningToastr('Su rol no tiene permisos asignados', this.TITLE)
        }
    }
}
