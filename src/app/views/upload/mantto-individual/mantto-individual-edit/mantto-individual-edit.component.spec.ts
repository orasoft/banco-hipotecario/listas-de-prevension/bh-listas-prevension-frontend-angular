import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManttoIndividualEditComponent } from './mantto-individual-edit.component';

describe('ManttoIndividualEditComponent', () => {
  let component: ManttoIndividualEditComponent;
  let fixture: ComponentFixture<ManttoIndividualEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManttoIndividualEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManttoIndividualEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
