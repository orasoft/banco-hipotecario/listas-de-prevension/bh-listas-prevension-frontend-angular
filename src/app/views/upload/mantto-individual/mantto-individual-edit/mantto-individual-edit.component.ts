import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {ParseFieldsService} from '../../../../services/parseFieldsService';
import {ToastrManager} from 'ng6-toastr-notifications';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-mantto-individual-edit',
  templateUrl: './mantto-individual-edit.component.html',
  styleUrls: ['./mantto-individual-edit.component.scss']
})
export class ManttoIndividualEditComponent implements OnInit {

  public TITLE = 'Actualización de Información de Persona';
  public profile;
  public camposConf = []
  public valueIdList;
  public fields: FieldBase<any>[];
  public listForm: FormGroup;

  constructor(
      private router: Router,
      private modalService: BsModalService,
      private apiService: ApiService,
      private parse: ParseFieldsService,
      private route: ActivatedRoute,
      private toastr: ToastrManager
  ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    this.valueIdList = this.route.snapshot.paramMap.get('idList');
    console.log(this.route.snapshot.paramMap.get('idList'));
    console.log(this.route.snapshot.paramMap.get('idTrx'));
  }

  ngOnInit() {
    this.fields = []
    this.getListPP();
  }

  public getListPP() {
    this.apiService.getListaCampos(this.valueIdList).subscribe(data => {
      console.log(data.camposConfigurados)
      this.camposConf = data.camposConfigurados
      data.camposConfigurados.forEach(campo => {
        this.fields.push({
          name: campo.campo.nombre,
          index: campo.campo.id,
          label: campo.campo.label,
          type: campo.campo.formato,
          required: campo.requerido,
          search: campo.busqueda,
          order: campo.orden,
          status: campo.estado,
          subcategories: campo.campo.subcategorias
        })
      })
      this.generateFields()
    })
  }

  public generateFields() {
    this.fields = this.parse.setFieldsForm(this.fields)
    this.listForm = this.parse.toFormGroup(this.fields)

    // if (!this.createRegister) {
    //
    //   this.apiService.getRegisterById(this.idListEdit).subscribe( data => {
    //     // console.log(data)
    //     this.fieldsEdit = data
    //     this.fieldsEdit.registro.forEach( reg => {
    //       console.log(reg)
    //       this.listForm.controls[reg.idField].setValue(reg.value)
    //     })
    //     console.log(this.fieldsEdit)
    //   })
    // }

    console.log(this.listForm)
  }

  public update() {

  }

  public cancel() {
    this.router.navigate(['/app/upload/mantto-individual/list'])
  }

}
