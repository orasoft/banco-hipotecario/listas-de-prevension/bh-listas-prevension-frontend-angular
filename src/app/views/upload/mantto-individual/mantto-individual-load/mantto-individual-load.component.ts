import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../../services/apiService';
import {IListaCatalog} from '../../../../interface/catalogs/lista-catalog';
import {IList} from '../../../../interface/ilist';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IRegisterList, IRegisterPerson} from '../../../../interface/iRegisterList';
import {ILista} from '../../../admin/mantto-listas/interfaces/ilista';
import * as moment from 'moment';
import {PaginationInstance} from 'ngx-pagination';
import {IPermPerfil} from '../../../../interface/iperm-perfil';
import {IRoleSelected} from '../../../../interface/irole-selected';

@Component({
    selector: 'app-mantto-individual-load',
    templateUrl: './mantto-individual-load.component.html',
    styleUrls: ['./mantto-individual-load.component.scss']
})
export class ManttoIndividualLoadComponent implements OnInit {

    public TITLE = 'Carga Individual'
    // public listaCatalog: IListaCatalog[]
    public listaCatalog = []
    public valueIdList: number

    public listaPP = [];
    public createStatus = true;

    public fieldsRegister = [];
    public camposHeaders = [];
    public date;
    public listaListFiltered = [];

    public permPerfil: IPermPerfil;
    public roleSelected: IRoleSelected;
    public profile;

    public configPage: PaginationInstance = {
        id: 'advanced',
        itemsPerPage: 10,
        currentPage: 1
    }

    constructor(
        private apiService: ApiService,
        private router: Router,
        private toastr: ToastrManager
    ) {
        this.profile = JSON.parse(localStorage.getItem('profile'));
        this.roleSelected = JSON.parse(localStorage.getItem('role'));
    }

    ngOnInit() {
        this.getListCatalog()
    }

    onPageChange(number: number) {
        this.configPage.currentPage = number;
    }

    private getPermisos() {

        this.apiService.getRolesList().subscribe(data => {
            const role = data.filter(item => Number(item.idBhRol) === Number(this.roleSelected.id))[0]
            this.apiService.getPermPerfil(`?rolId=${role.id}&listaId=${this.valueIdList}`).subscribe(perm => {
                this.permPerfil = perm[0]
                // console.log(this.permPerfil)
            })
        })
    }

    public getListPP() {
        if (this.valueIdList !== undefined) {

            if (this.permPerfil.acceso === 'Consulta' || this.permPerfil.acceso === 'Edicion') {

                this.apiService.listaPorIdRegistros(this.valueIdList).subscribe(data => {
                    this.toastr.successToastr('Información de la lista se ha cargado con éxito', this.TITLE);
                    this.listaPP = data
                    this.date = moment(data.ultimaCarga).format('YYYY-MM-DD')
                    this.camposHeaders = this.ordenLista(data.header);
                    this.fieldsRegister = []
                    data.filas.forEach(linea => {
                        const linesReg = []
                        linea.detalle.forEach(campo => {

                            if (this.camposHeaders.filter(item => item.campoId === campo.campo.id).length > 0) {
                                const values = this.camposHeaders.filter(item => item.campoId === campo.campo.id)[0]
                                linesReg.push({
                                    id: campo.campo.id,
                                    valor: campo.valor,
                                    orden: values.orden
                                })
                            }
                        })
                        this.fieldsRegister.push(linesReg);
                    })
                    this.createStatus = false
                })

            } else {
                this.toastr.warningToastr('No tiene permisos para consultar sobre la lista', this.TITLE)
            }
        } else {
            this.toastr.warningToastr('Debe seleccionar una lista', this.TITLE);
        }
    }

    public getRegList() {
        this.apiService.getRegisterList(this.valueIdList).subscribe(data => {

            this.fieldsRegister = data
            // console.log(this.fieldsRegister)
        })
    }

    private getListCatalog() {
        this.apiService.todasListas().subscribe(data => {
            this.listaCatalog = data.items
            this.listaCatalog.forEach( lista => {
                if (lista.responsables.find( item => item.id === this.profile.usuario.id ) !== undefined) {
                    this.listaListFiltered.push(lista);
                }
            });
            if (this.listaListFiltered.length === 0) {
                this.toastr.successToastr('No posee ninguna lista asociada', this.TITLE)
            }
        })
    }

    public createPerson() {
        if (this.permPerfil !== undefined) {
            if (this.permPerfil.acceso === 'Edicion') {
                this.router.navigate(['/app/upload/mantto-individual/create', {create: true, idList: this.valueIdList}])
            } else {
                this.toastr.warningToastr('Su rol no tiene permisos de edición para esta lista', this.TITLE)
            }
        } else {
            this.toastr.warningToastr('Su rol no tiene permisos asignados', this.TITLE)
        }
    }

    public onChange() {
        this.createStatus = true
        this.getPermisos()
    }

    public cancel() {
        this.router.navigate(['/app/upload/mantto-individual/list'])
    }

    private ordenLista(list) {
        return list.sort((a, b) => {
            return a.orden - b.orden
        })
    }

}
