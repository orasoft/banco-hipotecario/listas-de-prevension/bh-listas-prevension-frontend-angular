import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ApiService} from '../../../../services/apiService';
import {IList} from '../../../../interface/ilist';
import {FieldBase} from '../../../../interface/parseFields/field-base';
import {ParseFieldsService} from '../../../../services/parseFieldsService';
import {FormGroup} from '@angular/forms';
import {ToastrManager} from 'ng6-toastr-notifications';
import {IRegisterList, IRegisterPerson} from '../../../../interface/iRegisterList';
import {ILista} from '../../../admin/mantto-listas/interfaces/ilista';


@Component({
  selector: 'app-mantto-individual-create',
  templateUrl: './mantto-individual-create.component.html',
  styleUrls: ['./mantto-individual-create.component.scss']
})
export class ManttoIndividualCreateComponent implements OnInit {

  public TITLE = 'Registro de Persona'
  public modalRef: BsModalRef

  public fields: FieldBase<any>[]
  public listForm: FormGroup
  public listaPP: ILista

  public valueIdList;
  public fieldsRegister: IRegisterPerson[]
  public createRegister: boolean

  public fieldsEdit: IRegisterList
  public idListEdit: number

  public camposConf = []
  public profile;
  constructor(
      private router: Router,
      private modalService: BsModalService,
      private apiService: ApiService,
      private parse: ParseFieldsService,
      private route: ActivatedRoute,
      private toastr: ToastrManager
  ) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    console.log(this.route.snapshot.paramMap.get('create'))
    console.log(this.route.snapshot.paramMap.get('idList'))
    console.log(this.route.snapshot.paramMap.get('reg'))

    this.createRegister = this.stringToBool(this.route.snapshot.paramMap.get('create'))
    console.log(this.createRegister)
    if (this.route.snapshot.paramMap.get('create') === 'true') {
      this.valueIdList = this.route.snapshot.paramMap.get('idList')
      console.log('crear registro')
      console.log(this.valueIdList)
    } else {
      this.valueIdList = this.route.snapshot.paramMap.get('idList')
      console.log('editar registro')
      console.log(this.valueIdList)
      console.log(this.route.snapshot.paramMap.get('reg'))
      this.idListEdit = Number(this.route.snapshot.paramMap.get('reg'));

    }

  }

  ngOnInit() {
    this.fields = []
    this.fieldsRegister = []
    this.getListPP()
  }

  public cancel() {
    this.router.navigate(['/app/upload/mantto-individual/load'])
  }

  public getListPP() {
    this.apiService.getListaCampos(this.valueIdList).subscribe(data => {
      console.log(data.camposConfigurados)
      this.camposConf = data.camposConfigurados
      data.camposConfigurados.forEach(campo => {
        this.fields.push({
          name: campo.campo.nombre,
          index: campo.campo.id,
          label: campo.campo.label,
          type: campo.campo.formato,
          required: campo.requerido,
          search: campo.busqueda,
          order: campo.orden,
          status: campo.estado,
          subcategories: campo.campo.subcategorias
        })
      })
      this.generateFields()
    })
  }

  public generateFields() {
    this.fields = this.parse.setFieldsForm(this.fields)
    this.listForm = this.parse.toFormGroup(this.fields)

    if (!this.createRegister) {

      this.apiService.getRegisterById(this.idListEdit).subscribe( data => {
        // console.log(data)
        this.fieldsEdit = data
        this.fieldsEdit.registro.forEach( reg => {
          console.log(reg)
          this.listForm.controls[reg.idField].setValue(reg.value)
        })
        console.log(this.fieldsEdit)
      })
    }

    console.log(this.listForm)
  }

  public register() {
    if (this.createRegister) {
      this.save()
    } else {
      this.update()
    }
  }

  public save() {

    if (this.listForm.valid) {

      const listFormKeys = Object.keys(this.listForm.controls);
      listFormKeys.forEach( fieldForm => {
        console.log(this.listForm.controls[fieldForm].value)
        this.fieldsRegister.push({
          idField: fieldForm,
          order: this.findList(fieldForm).orden,
          value: this.listForm.controls[fieldForm].value
        })
      })

      const register: IRegisterList = {
        codLista: this.valueIdList,
        registro: this.fieldsRegister,
        masivoIndivudual: 'I',
        estado: true,
        observaciones: ''
      }

      const body = {
        listaId: this.valueIdList,
        usuarioId: this.profile.usuario.id,
        forma: 'I'
      }

      console.log(this.fieldsRegister);

      this.apiService.cargaMasiva(body).then( data => {

          this.apiService.transacciones({ cargaMasivaId: data.id}).then( trxs => {
            this.fieldsRegister.forEach( dato => {
              const bodydet = {
                campoId: dato.idField,
                transaccionId: trxs.id,
                valor: dato.value
              };
              this.apiService.transaccionesColumna(bodydet).then( () => {})
            })
            this.toastr.successToastr('Registro exitoso', this.TITLE);
            this.router.navigate(['/app/upload/mantto-individual/load'])
          })
      })
    } else {
      this.toastr.warningToastr('Favor complete los campos', this.TITLE)
    }

  }

  public update() {

    if (this.listForm.valid) {

      const listFormKeys = Object.keys(this.listForm.controls);
      listFormKeys.forEach( fieldForm => {
        console.log(fieldForm)
        console.log(this.listForm.controls[fieldForm].value)
        this.fieldsRegister.push({
          idField: Number(fieldForm),
          order: this.findList(fieldForm).orden,
          value: this.listForm.controls[fieldForm].value
        })
      })

      const register: IRegisterList = {
        id: this.fieldsEdit.id,
        codLista: this.fieldsEdit.codLista,
        registro: this.fieldsRegister,
        masivoIndivudual: this.fieldsEdit.masivoIndivudual,
        estado: this.fieldsEdit.estado,
        observaciones: this.fieldsEdit.observaciones
      }

      this.apiService.updatePerson(register).subscribe( data => {
        this.toastr.successToastr('Actualización exitosa', this.TITLE)
        this.router.navigate(['/app/upload/mantto-individual/load'])
      })
      console.log(this.fieldsRegister)
    } else {
      this.toastr.warningToastr('Favor complete los campos', this.TITLE)
    }

  }

  private findList(key) {
    console.log(key)
    return this.camposConf.find( ob => ob.campo.id === key)
  }

  private stringToBool(value): boolean {

    switch (value) {
      case 'true':
        return true
      case true:
        return true
      case 'false':
        return false
      case false:
        return false
    }
  }

}
