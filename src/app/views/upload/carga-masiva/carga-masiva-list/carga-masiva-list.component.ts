import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import * as XLSX from 'xlsx';
import {ApiService} from '../../../../services/apiService';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ILista} from '../../../admin/mantto-listas/interfaces/ilista';
import {NgxSpinnerService} from 'ngx-spinner';
import {ExcelService} from '../../../../services/excelService';
import * as moment from 'moment';
import {PaginationInstance} from 'ngx-pagination';
import {ICampos} from '../../../../interface/icampos';
import {IRoleSelected} from '../../../../interface/irole-selected';
import {IPermPerfil} from '../../../../interface/iperm-perfil';

type AOA = any[][];

@Component({
    selector: 'app-carga-masiva-list',
    templateUrl: './carga-masiva-list.component.html',
    styleUrls: ['./carga-masiva-list.component.scss']
})
export class CargaMasivaListComponent implements OnInit {

    public TITLE = 'Carga Masiva de Listas'
    public modalRef: BsModalRef
    public valueList: number
    // public listaPP: IList
    public listaPP: ILista
    public headFields = []
    public headFieldsExport = []
    // public listaList: IListaCatalog[]
    public listaList = [];
    public listaListFiltered = [];
    public selectStatus = false
    public listAreas = [];
    public profile;
    public selectArea;
    public valueArea;
    public listRegUpload = [];
    public listregs = [];

    public solicitadoPor
    public cargadoPor
    public fechaCreacion
    public cantRegs;
    data: AOA;
    newData: AOA;
    public validFile = true

    arrayBuffer: any;
    file

    public p = 1;

    public configPage: PaginationInstance = {
        id: 'advanced',
        itemsPerPage: 10,
        currentPage: 1
    }
    public errors = []
    public validCampos = []
    public roleSelected: IRoleSelected
    public permPerfil: IPermPerfil

    constructor(
        private modalService: BsModalService,
        private apiService: ApiService,
        private router: Router,
        public toastr: ToastrManager,
        private spinner: NgxSpinnerService,
        private excelService: ExcelService
    ) {
        // this.profile = JSON.parse(localStorage.getItem('profile'))
        this.profile = JSON.parse(localStorage.getItem('profile'));
        this.roleSelected = JSON.parse(localStorage.getItem('role'))
        // console.log(this.profile)
    }

    ngOnInit() {
        this.getList()
        this.getAreas()
    }

    public getAreas() {
        this.apiService.getAreas().subscribe(data => {
            // console.log(data)
            this.listAreas = data.items;
        })
    }

    onPageChange(number: number) {
        this.configPage.currentPage = number;
    }

    public hideSpinner() {
        setTimeout(() =>
                this.spinner.hide(),
            this.toastr.successToastr('Archivo importado con exito', this.TITLE)
            ,
            5000);
    }

    public exportExcel() {

        const data = [];
        const head = {};
        this.headFieldsExport.forEach(reg => {
            if (reg.estado === 'A') {
                head[reg.campo.label] = '';
            }
        })
        data.push(head)
        this.excelService.exportAsExcelFile(data, 'plantilla');
    }

    private getList() {
        this.listaListFiltered = [];
        this.apiService.todasListas().subscribe(data => {
            this.listaList = data.items;
            this.listaList.forEach( lista => {
                if (lista.responsables.find( item => item.id === this.profile.usuario.id ) !== undefined) {
                    this.listaListFiltered.push(lista);
                }
            });
            if (this.listaListFiltered.length === 0) {
                this.toastr.successToastr('No posee ninguna lista asociada', this.TITLE)
            }
            // console.log(this.listaList)
        })
    }

    public onChange() {
        this.headFieldsExport = []
        this.headFields = []
        this.listRegUpload = []
        this.selectStatus = false;
        this.getPermisos()
        // if (this.valueList > 0) {
        // }
    }

    private getPermisos() {
        this.apiService.getRolesList().subscribe(data => {
            const role = data.filter(item => Number(item.idBhRol) === Number(this.roleSelected.id))[0]
            this.apiService.getPermPerfil(`?rolId=${role.id}&listaId=${this.valueList}`).subscribe(perm => {
                this.permPerfil = perm[0]
            })
        })
    }

    public selectedList(template) {

        if (this.valueList !== undefined) {
            if (this.permPerfil.acceso === 'Edicion') {
                this.modalRef = this.modalService.show(template, {
                    class: 'modal-primary',
                    backdrop: true,
                    ignoreBackdropClick: true,
                    keyboard: false
                })
                this.selectStatus = true
                this.apiService.getListaPorId(this.valueList).subscribe(data => {
                    // console.log(data)
                    // console.log(this.fieldSearch(data.data))
                    this.listaPP = data
                    this.headFieldsExport = this.orderList(this.listaPP.camposConfigurados)
                    // console.log(this.headFields)
                    // console.log(this.listaPP)
                })
                this.apiService.getListaCampos(this.valueList).subscribe(data => {
                    this.validCampos = this.ordenLista(data.camposConfigurados)
                    console.log('campos')
                    console.log(this.validCampos)
                })
                this.ultCarga();
            } else {
                this.toastr.warningToastr('No tiene permisos de edición sobre la lista', this.TITLE)
            }
        } else {
            this.toastr.warningToastr('Debe seleccionar una lista', this.TITLE);
        }
    }

    private ultCarga() {
        this.apiService.ultimaCarga(this.valueList).subscribe(data => {

            if (data.filas.length > 0) {
                this.toastr.successToastr('Información de Lista cargada con éxito', this.TITLE);
                this.solicitadoPor = data.solitadoPor;
                this.cargadoPor = data.cargadoPor;
                this.fechaCreacion = moment(data.fechaCarga).format('DD/MM/YYYY')
                this.cantRegs = data.total;

                const head = [];
                this.headFieldsExport.forEach( h => {
                    if (h.estado === 'A') {
                        head.push({
                            campoId: h.campo.id,
                            label: h.campo.label,
                            orden: h.orden
                        })
                    }
                });
                this.headFields = this.ordenLista(head);
                this.listRegUpload = []
                data.filas.forEach(linea => {
                    const linesReg = []
                    linea.detalle.forEach(campo => {

                        if (this.headFields.filter(item => item.campoId === campo.campo.id).length > 0) {
                            const values = this.headFields.filter(item => item.campoId === campo.campo.id)[0]
                            linesReg.push({
                                id: campo.campo.id,
                                valor: campo.valor,
                                orden: values.orden
                            })
                        }
                    })
                    this.listRegUpload.push(linesReg);
                })
            } else {
                this.toastr.warningToastr('La lista seleccionada no posee cargas', this.TITLE);
            }

        })
    }

    private ordenLista(list) {
        return list.sort((a, b) => {
            return a.orden - b.orden
        })
    }

    private orderList(list) {
        return list.sort((a, b) => {
            return a.orden - b.orden
        })
    }

    public onFileChange(evt: any) {
        /* wire up file reader */
        console.log(evt)
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) {
            throw new Error('Cannot use multiple files');
        }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbook */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];

            /* save data */
            this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
            const lines = [];
            for (let i = 0; i < this.data.length; i++) {
                if (i > 0) {
                    lines.push(this.data[i])
                }
            }
            this.data = lines
            console.log(lines)
        };
        reader.readAsBinaryString(target.files[0]);
    }

    public validate(template, templateError) {
        this.validFile = true
        if (this.valueArea !== undefined && this.data !== undefined) {
            this.selectArea = {
                id: this.valueArea,
                name: this.findName(this.valueArea)
            }
            this.closeModal();
            this.newData = []
            for (let i = 0; i < 10; i++) {
                this.newData[i] = this.data[i]
            }

            const heads = [];
            this.headFieldsExport.forEach( h => {
                if (h.estado === 'A') {
                    heads.push(h);
                }
            });
            this.headFieldsExport = heads;
            if (this.headFieldsExport.length !== this.newData[0].length) {
                this.validFile = false
                this.toastr.warningToastr('El archivo seleccionado no tiene la misma cantidad de campos que la lista', this.TITLE);
            }
            let i = 0;
            this.errors = []

            const headsValid = [];
            this.validCampos.forEach( h => {
                if (h.estado === 'A') {
                    headsValid.push(h);
                }
            });
            this.validCampos = headsValid;

            this.data.forEach(values => {
                let j = 0;
                console.log('values.length');
                console.log(values.length);
                console.log(this.validCampos.length);
                console.log(this.validCampos)
                if (values.length === this.validCampos.length) {
                    let resp
                    for (const field of values) {
                        if (field === undefined) {
                            if (this.validCampos[j].requerido) {
                                this.errors.push({
                                    valor: 'Sin dato',
                                    desc: 'No se permiten valores nulos',
                                    fila: i + 1,
                                    columna: j + 1,
                                    campo: this.validCampos[j].campo.label,
                                    tipoCampo: this.validCampos[j].campo.formato
                                })
                            } else {
                                this.data[i][j] = '';
                            }
                        } else {
                            resp = this.validateField(this.validCampos[j].campo.formato, field, this.validCampos[j].campo.subcategorias)
                            if (this.validCampos[j].campo.formato === 'scat') {
                                this.data[i][j] = this.data[i][j].toUpperCase()
                            }
                            if (!resp) {
                                this.errors.push({
                                    valor: field,
                                    desc: 'Valor Incorrecto',
                                    fila: i + 1,
                                    columna: j + 1,
                                    campo: this.validCampos[j].campo.label,
                                    tipoCampo: this.validCampos[j].campo.formato
                                })
                            }
                        }
                        j++;
                    }
                } else {
                    this.toastr.warningToastr('Hay campos vacios en la fila', this.TITLE);
                }
                i++;
            })
            console.log(this.errors)
            if (this.errors.length > 0) {
                this.modalRef = this.modalService.show(templateError, {
                    class: 'modal-primary modal-lg',
                    backdrop: true,
                    ignoreBackdropClick: true,
                    keyboard: false
                })
            } else {
                this.modalRef = this.modalService.show(template, {
                    class: 'modal-primary modal-lg',
                    backdrop: true,
                    ignoreBackdropClick: true,
                    keyboard: false
                })
            }
        } else {
            this.toastr.warningToastr('Debe seleccionar los datos requeridos', this.TITLE);
        }
    }

    public validateField(formato, field, values): boolean {
        switch (formato) {
            case 'num': {
                if (!isNaN(field)) {
                    return true
                } else {
                    return false
                }
            }
            case 'text': {
                return true
            }
            case 'date': {
                if (moment(field, 'YYYY-MM-DD', true).isValid()) {
                    return true
                } else {
                    return false
                }
            }
            case 'scat': {
                let flag = false
                values.forEach(val => {
                    if (field.toLowerCase() === val.valor.toLowerCase()) {
                        flag = true
                    }
                })
                return flag
            }
        }
    }

    public importData() {
        this.modalRef.hide()
        this.spinner.show()
        const importList = []

        this.data.forEach(lineData => {
            const linesRegister = []
            let i = 0;
            this.headFieldsExport.forEach(head => {
                linesRegister.push({
                    idField: head.campo.id,
                    order: head.orden,
                    value: lineData[i]
                })
                i++;
            })
            const register = {
                registro: linesRegister,
            }
            importList.push(register)
        })

        const body = {
            listaId: this.valueList,
            areaId: this.valueArea,
            usuarioId: this.profile.usuario.id,
            forma: 'M'
        }
        // console.log(this.data)
        this.spinner.show()
        this.apiService.cargaMasiva(body).then(data => {

            importList.forEach(line => {
                this.apiService.transacciones({cargaMasivaId: data.id}).then(trxs => {

                    return new Promise((resolve, reject) => {
                        const colaPeticiones = [];
                        line.registro.forEach(campo => {
                            colaPeticiones.push(this.enviarCampo(campo.idField, trxs.id, campo.value));
                        });
                        const envioCampos = Promise.all(colaPeticiones);
                        resolve(envioCampos)
                        this.ultCarga();
                    });

                })
            })

        })
        this.hideSpinner()
    }

    public verifyFile(confirm) {
        this.modalRef.hide()
        this.modalRef = this.modalService.show(confirm, Object.assign({
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        }))
    }

    enviarCampo(campoId, trxId, valor) {

        return new Promise((resolve, reject) => {
            const body = {
                campoId: campoId,
                transaccionId: trxId,
                valor: valor
            }
            this.apiService.transaccionesColumna(body).then(column => {
                console.log(column)
                resolve(column)
            })
        })
    }

    public closeModal() {
        this.validFile = true
        this.modalRef.hide()
    }

    public cerrarMensaje() {
        this.modalRef.hide()
    }

    public upFileList(template) {
        this.modalRef = this.modalService.show(template, {
            class: 'modal-primary',
            backdrop: true,
            ignoreBackdropClick: true,
            keyboard: false
        })
    }

    public goConsult() {
        this.router.navigate(['/app/upload/carga-masiva/consulta'])
    }

    public findName(id) {
        return this.listAreas.filter(item => item.id === id)[0].nombre
    }
}
