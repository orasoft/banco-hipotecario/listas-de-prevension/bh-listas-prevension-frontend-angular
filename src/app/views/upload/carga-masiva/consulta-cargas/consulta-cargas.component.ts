import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ApiService} from '../../../../services/apiService';
import * as moment from 'moment';

@Component({
  selector: 'app-consulta-cargas',
  templateUrl: './consulta-cargas.component.html',
  styleUrls: ['./consulta-cargas.component.scss']
})
export class ConsultaCargasComponent implements OnInit{

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtLanguage: DataTables.LanguageSettings = {
    search: 'Buscar',
    lengthMenu: 'Mostrar _MENU_ registros por página',
    info: 'Mostrando página _PAGE_ de _PAGES_',
    infoFiltered: '(Filtrado de _MAX_ registros)',
    paginate: {
      first: 'Primero',
      last: 'Último',
      next: 'Siguiente',
      previous: 'Anterior'
    },
    emptyTable: 'No se encontraron resultados'
  };

  dtTrigger: Subject<any> = new Subject();

  public TITLE = 'Consulta de Cargas'
  public modalRef: BsModalRef
  public listaCargas = [];

  public motivo: string
  public idCargaBaja: string

  constructor(
      private router: Router,
      private modalService: BsModalService,
      private apiService: ApiService,
  ) {

  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: this.dtLanguage,
      stateSave: false,
      ordering: false
    };

    this.apiService.getCargaMasiva().subscribe( data => {
      this.listaCargas = data
      this.listaCargas.forEach( item => {
        item.fechaCarga = moment(item.fechaCarga).format('DD/MM/YYYY')
      })
      this.dtTrigger.next()
    })
  }

  public returnPage() {
    this.router.navigate(['/app/upload/carga-masiva/list'])
  }

  public rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  public modalAnulation(template, id) {
    this.idCargaBaja = id;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    })
  }

  public closeModal() {
    this.modalRef.hide()
  }

  public darBaja() {
    const body = {
      motivo: this.motivo
    }
    this.apiService.updateCarga(this.idCargaBaja, body).subscribe( data => {
      this.modalRef.hide()
      this.apiService.getCargaMasiva().subscribe( up => {
        this.listaCargas = up
        this.listaCargas.forEach( item => {
          item.fechaCarga = moment(item.fechaCarga).format('DD/MM/YYYY')
        })
        this.rerender()
      })
    })
  }

}
