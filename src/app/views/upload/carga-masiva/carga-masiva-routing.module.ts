import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CargaMasivaListComponent} from './carga-masiva-list/carga-masiva-list.component';
import {ConsultaCargasComponent} from './consulta-cargas/consulta-cargas.component';


export const routes: Routes = [
    {
        path: 'list',
        component: CargaMasivaListComponent,
        data: {
            title: 'Carga Masiva de Listas'
        }

    },
    {
        path: 'consulta',
        component: ConsultaCargasComponent,
        data: {
            title: 'Consulta de cargas'
        }

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CargaMasivaRoutingModule {
}
