import {NgModule} from '@angular/core';
import {UploadRoutingModule} from './upload-routing.module';

@NgModule({
    imports: [UploadRoutingModule]
})

export class UploadModule {
}
