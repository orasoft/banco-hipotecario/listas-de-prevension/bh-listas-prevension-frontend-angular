import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

  private profile;
  public usuario
  public rol

  constructor( private router: Router,
               private toastr: ToastrManager) {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    this.rol = JSON.parse(localStorage.getItem('role'));
  }

  public logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    localStorage.removeItem('profile')
    this.router.navigate(['/login'])
    this.toastr.successToastr('Cierre de sesión exitoso', 'Cierre de Sesión');
  }

}


