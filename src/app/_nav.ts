export const navigation = [
  {
    title: true,
    name: 'Menu Principal'
  },
  {
    name: 'Inicio',
    url: '/app/home',
    icon: 'fa fa-home'
  },
  {
    name: 'Administración',
    url: '/app/admin',
    icon: 'fa fa-folder',
    children: [
      {
        name: 'Mantenimiento de Listas',
        url: '/app/admin/mantto-listas/list',
        icon: 'fa fa-bars'
      },
      {
        name: 'Asignación de Perfiles',
        url: '/app/admin/asig-perfiles/list',
        icon: 'fa fa-id-card-o'
      },
      {
        name: 'Mantenimiento de Áreas',
        url: '/app/admin/mantto-areas/list',
        icon: 'fa fa-th-large'
      },
      {
        name: 'Conexión a Listas Externas ',
        url: '/app/admin/conex-list/conex-list-ext',
        icon: 'fa fa-plug'
      }
    ]
  },
  {
    name: 'Carga de información',
    url: '/base',
    icon: 'fa fa-upload',
    children: [
      {
        name: 'Carga Masiva de Listas',
        url: '/app/upload/carga-masiva/list',
        icon: 'fa fa-angle-double-up'
      },
      {
        name: 'Mantienimiento Individual de Listas',
        url: '/app/upload/mantto-individual/list',
        icon: 'fa fa-angle-up'
      }
    ]
  },
  {
    name: 'Consultas/Reportes',
    url: '/base',
    icon: 'fa fa-file-text-o',
    children: [
      {
        name: 'Consulta Historica de Información de Listas',
        url: '/app/reports/consulta-historica/list',
        icon: 'fa fa-file-text'
      },
      {
        name: 'Consulta Individual de Persona',
        url: '/app/reports/consulta-individual/list',
        icon: 'fa fa-user'
      }
    ]
  },
];
