import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';

// Import containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

const APP_CONTAINERS = [
  FullLayoutComponent,
  SimpleLayoutComponent
]

// Import components
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
]

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
]

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import {ModalModule} from 'ngx-bootstrap';
import {ToastrModule} from 'ng6-toastr-notifications';
import {SharedModule} from './views/shared/shared.module';
import {AuthService} from './services/authService';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ApiService} from './services/apiService';
import { NgSelectModule } from '@ng-select/ng-select';
import {ParseFieldsService} from './services/parseFieldsService';
import {ApiInterceptor} from './services/api.interceptor';
import {ExcelService} from './services/excelService';
import {DataTablesModule} from 'angular-datatables';
import {AccordionModule} from 'angularx-accordion';
import {ExportAsModule} from 'ngx-export-as';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ShContextMenuModule} from 'ng2-right-click-menu';
import {NgxPaginationModule} from 'ngx-pagination';
import {AppError404Component} from './views/errors/app-error-404/app-error-404.components';
import {AuthGuardService} from './services/auth-guard-service';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    SharedModule,
    NgxSpinnerModule,
    NgSelectModule,
    DataTablesModule,
    AccordionModule,
    ExportAsModule,
    NgxDatatableModule,
    NgxPaginationModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
    AppError404Component
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },
              { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
      AuthService, AuthGuardService, ApiService, ParseFieldsService, ExcelService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
