import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Usuario} from '../views/loginForm/usuario';
import {Observable} from 'rxjs/Observable';
import {NgxSpinnerService} from 'ngx-spinner';
import {finalize} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {ILogin} from '../interface/login/ilogin';
import {IRole} from 'app/interface/login/irole';

@Injectable()
export class AuthService {

    private url = environment.apiUrl;

    constructor(private http: HttpClient,
                private spinner: NgxSpinnerService) {
    }

    login(usuario: Usuario): Observable<any> {

        this.spinner.show()

        const body = {
            username: usuario.username,
            password: usuario.password
        }
        return this.http.post<any>(`${this.url}/api/v1/auth`, body)
            .pipe(finalize(() => {this.hideSpinner()}));

    }

    getRoles(): Observable<IRole> {
        return this.http.get<IRole>(`${this.url}/api/v1/session/me`).pipe(finalize(() => {
            this.hideSpinner()
        }));
    }

    getMenu(id): Observable<any> {
        return this.http.get<any>(`${this.url}/usuarios/menu/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }));
    }

    public hideSpinner() {
        setTimeout(() =>
                this.spinner.hide(),
            1000);
    }
}
