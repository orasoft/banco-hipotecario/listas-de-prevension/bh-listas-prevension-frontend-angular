import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {finalize} from 'rxjs/operators';
import {IList} from '../interface/ilist';
import {IListaCatalog} from '../interface/catalogs/lista-catalog';
import {IUsuarioCatalog} from '../interface/catalogs/usuario-catalog';
import {IRegisterList} from '../interface/iRegisterList';
import {IRoleCatalog} from '../interface/catalogs/role-catalog';
import {ICampos} from '../views/admin/mantto-listas/mantto-campos/ICampo';


@Injectable()
export class ApiService {

    private url = environment.apiUrl;

    // private urlNew = 'http://10.70.95.105:3000/api/v1';
    private urlNew = `${this.url}/api/v1`;

    constructor(private http: HttpClient,
                private spinner: NgxSpinnerService) {
    }

    public hideSpinner() {
        setTimeout(() =>
                this.spinner.hide(),
            1000);
    }

    // ENDPOINTS PRUEBA
    public getAllCampos(): Observable<ICampos> {
        this.spinner.show()
        return this.http.get<ICampos>(`${this.urlNew}/config/campos`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getCamposPredet(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/campos/predeterminados`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public createCampo(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/campos`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateCampo(id, body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/config/campos/${id}`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public crearLista(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/listas`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public actLista(body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/config/listas`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public asociarCampo(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/listas/asociar/masivo`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public asociarCampoAct(body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/config/listas/asociar/masivo/act`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public todasListas(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/listas`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListaFiltro(values) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/listas/consulta${values}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListaById(values) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/listas/consulta${values}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListaCampos(values) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/listas/campos/${values}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getUserCatalog(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/usuarios`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRolesList(): Observable<any> {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/roles`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public crearPerfil(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/perfiles`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getPerfiles(id) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/listas/perfiles/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updatePerfiles(body): Observable<any> {
        this.spinner.show();
        return this.http.put<any>(`${this.urlNew}/config/perfiles`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListasConex() {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/conexion_externa`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public crearConexList(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/conexion_externa`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public eliminarListasConex(id) {
        this.spinner.show()
        return this.http.delete<any>(`${this.urlNew}/config/conexion_externa/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getAreas(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/areas`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getAreasId(id): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/areas/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public crearArea(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/areas`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public regUserArea(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/areas/responsable`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListasExternas() {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/config/listas/externas`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public removeUserArea(body): Observable<any> {
        console.log(body)
        this.spinner.show()
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: body
        };
        return this.http.delete<any>(`${this.urlNew}/config/areas/responsable`, httpOptions).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateArea(body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/config/areas/`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public removeArea(id): Observable<any> {
        this.spinner.show()
        return this.http.delete<any>(`${this.urlNew}/config/areas/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public cargaMasiva(body): Promise<any> {
        // this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/transaccional/carga`, body).toPromise()
        //     .pipe(finalize(() => {
        //     this.hideSpinner()
        // }))
    }

    public transacciones(body): Promise<any> {
        // this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/transaccional/transacciones`, body).toPromise()
        //     .pipe(finalize(() => {
        //     this.hideSpinner()
        // }))
    }

    public transaccionesColumna(body): Promise<any> {
        // this.spinner.show()
        return this.http.post(`${this.urlNew}/transaccional/columnas`, body).toPromise()
        //     .pipe(finalize(() => {
        //     this.hideSpinner()
        // }))
    }

    public transaccionesColumnaById(id) {
        this.spinner.show()
        return this.http.get(`${this.urlNew}/transaccional/columnas/${id}`)
            .pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getPermPerfil(values) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/perfiles/consulta${values}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getCamposPerfil(id) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/perfiles/${id}/campos`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public agregarCampoPerfil(body) {
        this.spinner.show()
        return this.http.post<any>(`${this.urlNew}/config/perfiles/agregar/campo`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public quitarCampoPerfil(body) {
        this.spinner.show()
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: body
        };
        return this.http.delete<any>(`${this.urlNew}/config/perfiles/eliminar/campo`, httpOptions).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getListaPorId(id) {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/config/listas/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRegistros(body): Observable<any> {
        this.spinner.show();
        return this.http.post<any>(`${this.urlNew}/transaccional/transacciones/consulta`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getCargaMasiva(): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.urlNew}/transaccional/carga`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateCarga(id, body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/transaccional/carga/${id}`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public listaPorIdRegistros(id): Observable<any> {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/transaccional/transacciones/consulta/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public ultimaCarga(id): Observable<any> {
        this.spinner.show();
        return this.http.get<any>(`${this.urlNew}/transaccional/transacciones/ultimacarga/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public anularRegistro(id, body) {
        this.spinner.show()
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: body
        };
        return this.http.delete<any>(`${this.urlNew}/transaccional/transacciones/${id}`, httpOptions).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public bajaLista(id, body): Observable<any> {
        this.spinner.show()
        return this.http.put<any>(`${this.urlNew}/config/listas/desactivar/${id}`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // ENDPOINTS PRUEBA

    // Estructuras de Listas

    // public createList(body): Observable<IList> {
    //     this.spinner.show()
    //     return this.http.post<IList>(`${this.url}/listaspreven`, body).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // public getList(): Observable<any> {
    //     this.spinner.show()
    //     return this.http.get<any>(`${this.url}/listaspreven`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    public getListById(id: number): Observable<IList> {
        this.spinner.show()
        return this.http.get<IList>(`${this.url}/listaspreven/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // public getListFilter(idList, idUser) {
    //     this.spinner.show();
    //     return this.http.get<any>(`${this.url}/listaspreven/filter/${idList}/${idUser}`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // Estructuras de Listas

    // Otros

    // public registerProfile(body): Observable<any> {
    //     this.spinner.show()
    //     return this.http.post<any>(`${this.url}/profiles`, body).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    public getProfileById(id: number): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/profiles/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // public registerArea(body): Observable<any> {
    //     this.spinner.show()
    //     return this.http.post<any>(`${this.url}/areas`, body).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // public getAllArea(): Observable<any> {
    //     this.spinner.show()
    //     // @ts-ignore
    //     return this.http.get<any>(`${this.url}/areas`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // public regUserArea(body): Observable<any> {
    //     this.spinner.show()
    //     return this.http.post<any>(`${this.url}/areas/resp`, body).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    public getUserAreaById(id: number): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/areas/resp/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // public updateArea(body): Observable<any> {
    //     this.spinner.show()
    //     return this.http.post<any>(`${this.url}/areas/update`, body).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // public removeUserArea(id: number): Observable<any> {
    //     this.spinner.show()
    //     return this.http.delete<any>(`${this.url}/areas/${id}`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    public getAllListConex() {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/conexlist`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public registerConexList(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/conexlist`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updateConexList(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/conexlist/update`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public uploadReg(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/upload`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getUltimUp(id): Observable<any> {
        this.spinner.show()
        return this.http.get<any>(`${this.url}/upload/ultimate/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // Otros

    // Registros en Listas

    public registerPerson(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/lista/registro`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public updatePerson(body): Observable<any> {
        this.spinner.show()
        return this.http.post<any>(`${this.url}/lista/registro/update`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRegisterList(id: number): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.get<IRegisterList[]>(`${this.url}/lista/registro/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public getRegisterById(id: number): Observable<IRegisterList> {
        this.spinner.show()
        return this.http.get<IRegisterList>(`${this.url}/lista/registro/find/${id}`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public filterAllList(body): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.post<IRegisterList[]>(`${this.url}/lista/registro/filter`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    public filterByList(body): Observable<IRegisterList[]> {
        this.spinner.show()
        return this.http.post<IRegisterList[]>(`${this.url}/lista/registro/filter/list`, body).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // Registros en Listas

    // request catalogs

    public getListCatalog(): Observable<IListaCatalog[]> {
        this.spinner.show()
        return this.http.get<IListaCatalog[]>(`${this.url}/listaspreven/catalog`).pipe(finalize(() => {
            this.hideSpinner()
        }))
    }

    // public getUserCatalog(): Observable<any> {
    //     this.spinner.show()
    //     return this.http.get<any>(`${this.url}/usuarios`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // public getRolesList(): Observable<IRoleCatalog> {
    //     this.spinner.show();
    //     return this.http.get<IRoleCatalog>(`${this.url}/usuarios/roles`).pipe(finalize(() => {
    //         this.hideSpinner()
    //     }))
    // }

    // request catalogs
}
