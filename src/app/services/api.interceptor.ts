import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/do'
import {ToastrManager} from 'ng6-toastr-notifications';
import {Router} from '@angular/router';


@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    private urlRequest: string;
    private cloned: HttpRequest<any>;

    constructor(private toastr: ToastrManager,
                private router: Router) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.urlRequest = req.url;
        const token = localStorage.getItem('token');
        const url = '/auth'

        this.cloned = req.clone()

        if (token) {
            if (req.url.search(url) === -1) {
                this.cloned = req.clone( {
                    headers: req.headers.set('Authorization', 'Bearer ' + token)
                } )
            }
        }

        return next.handle(this.cloned).do(
            (ev: HttpEvent<any>) => {
                if (ev instanceof HttpResponse) {
                    if (ev.status === 200) {}
                }
            }
        ).catch( response => {
            if (response instanceof HttpErrorResponse) {
                switch (true) {
                    case (response.status === 401):
                        localStorage.removeItem('token');
                        localStorage.removeItem('profile');
                        localStorage.removeItem('role')
                        this.toastr.warningToastr('Inicie Sesión Nuevamente', 'Sesión Expirada')
                        this.router.navigate(['/login']);
                }
            }
            return Observable.throw(response);
        })
    }
}
